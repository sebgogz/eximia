const {
  override,
  fixBabelImports,
  addLessLoader,
} = require('customize-cra');

module.exports = override(
/*   addWebpackResolve({
    alias: {
      common: path.join(__dirname, 'src/common'),
    }
  }),
  addBabelPlugin('module-resolver', {
    alias: {
      '~common': './src/common',
    },
  }), */
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true,
  }),
  addLessLoader({
    javascriptEnabled: true,
  }),
);
