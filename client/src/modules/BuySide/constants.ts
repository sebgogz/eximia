export const accountColumns = [
  {
    title: 'ID',
    dataIndex: 'id',
  },
  {
    title: 'Client ID',
    dataIndex: 'clientId',
  },
  {
    title: 'Name',
    dataIndex: 'clientName',
  },
  {
    title: 'LastName',
    dataIndex: 'clientLastN',
  },
  {
    title: 'Mail',
    dataIndex: 'clientMail',
  },
  {
    title: 'Client Phone',
    dataIndex: 'clientPhone',
  },
];

export const accountInfoColumns = [
  {
    title: 'Account ID',
    dataIndex: 'accountId',
  },
  {
    title: 'Balance',
    dataIndex: 'balance',
  },
];
