import React from 'react';
import { Row, Col, Button } from 'antd';

function BuySideActions() {
  return (
    <Row
      type="flex"
      justify="end"
      gutter={8}
      className="buy-side-actions"
    >
      <Col>
        <Button>
          Recalculate
        </Button>
      </Col>
      <Col>
        <Button type="primary">
          Add Strategy
        </Button>
      </Col>
    </Row>
  );
}

export default BuySideActions;
