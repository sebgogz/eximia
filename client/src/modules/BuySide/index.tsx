import React from 'react';
import { Card, Table } from 'antd';

import { useGetAccount, useGetAccounts } from '../../apollo/hooks';
import ApolloError from '../../apollo/components/Error';

import { BuySideActions } from './components';

import { accountColumns, accountInfoColumns } from './constants';

import './BuySide.scss';

type BuySideProps = {
  accountId: string,
};

const renderExpandedRow = (account: any) => {
  const accountInfoData = account && account.accountInfo;

  return (
    <Table
      rowKey="accountId"
      pagination={false}
      columns={accountInfoColumns}
      dataSource={accountInfoData || []}
    />
  );
}

const BuySide: React.FC<BuySideProps> = ({ accountId }) => {
  const {
    data: accountData,
    loading: accountLoading,
    error: accountError,
  } = useGetAccount({});
  const {
    data: accountsData,
    loading: accountsLoading,
    error: accountsError,
  } = useGetAccounts({});
  
  if (accountError || accountsError) {
    return (
      <ApolloError error={accountError || accountsError} />
    );
  }

  const account = accountData && accountData.getAccount;
  const accounts = accountsData && accountsData.getAccounts;

  return (
    <>
      <BuySideActions />
      <Card title="Account Info">
        <Table
          rowKey="id"
          loading={accountLoading}
          columns={accountColumns}
          pagination={false}
          dataSource={account ? [account] : []}
          expandedRowRender={renderExpandedRow}
        />
      </Card>
      <Card title="Graphs and Statistics of Client">
        <Table
          rowKey="id"
          loading={accountsLoading}
          columns={accountColumns}
          dataSource={accounts || []}
          expandedRowRender={renderExpandedRow}
        />
      </Card>
    </>
  );
}

export default BuySide;
