import React from 'react';
import { Row, Col, Card, Form, Input, Button, Select } from 'antd';
import { FormComponentProps } from 'antd/es/form';

import { useAuth } from '../../context/auth-context';
import useHistory from '../../common/hooks/useHistory';

import urlService from '../../services/urlService';

import routes from '../../routes';

const { users } = routes;
const { Item } = Form;
const { Option } = Select;

interface RegisterProps extends FormComponentProps {
  email: string,
  password: string,
};

function RegisterForm(props: RegisterProps) {
  const history = useHistory();
  const { register } = useAuth();
  const { form } = props;
  const { getFieldDecorator, validateFields } = form;

  function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    
    validateFields((err, values) => {
      if (!err) {
        register(values);
        urlService.push(history, users);
      }
    });
  }

  return (
    <Card title="Login" >
      <Form onSubmit={handleSubmit}>
        <Item>
          {getFieldDecorator('email', {
            rules: [{
              required: true,
              message: 'Please input a valid email',
            }],
          })(
            <Input placeholder="Email" />
          )}
        </Item>
        <Item>
          {getFieldDecorator('password', {
            rules: [{
              required: true,
              message: 'Please input a valid password',
            }],
          })(
            <Input
              type="password"
              placeholder="Password"
            />
          )}
        </Item>
        <Item>
          {getFieldDecorator('role', {
            rules: [{
              required: true,
              message: 'Selecciona un rol',
            }],
          })(
            <Select placeholder="Role">
              <Option value="admin">Administrador</Option>
              <Option value="editor">Editor</Option>
              <Option value="normal">Normal</Option>
            </Select>
          )}
        </Item>
        <Row type="flex" justify="end">
          <Col>
            <Button
              type="primary"
              htmlType="submit"
            >
              Registrar
            </Button>
          </Col>
        </Row>
      </Form>
    </Card>
  );
}

const Register = Form.create({ name: 'register-form' })(RegisterForm);

export default Register;
