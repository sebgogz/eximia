import React from 'react';
import { Row, Col, Card, Form, Input, Button, Icon } from 'antd';
import { FormComponentProps } from 'antd/es/form';

import { useAuth } from '../../context/auth-context';

import './Login.scss';

const { Item } = Form;

interface LoginProps extends FormComponentProps {
  email: string,
  password: string,
};

function LoginForm(props: LoginProps) {
  const { form } = props;
  const { getFieldDecorator, validateFields } = form;
  const { login } = useAuth();

  function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    
    validateFields((err, values) => {
      const { email, password } = values;
      const params = { email, password }
      
      if (!err) {
        login(params);
      }
    });
  }

  return (
    <Row type="flex" justify="center">
      <Col span={6}>
        <Card title="Login" >
          <Form onSubmit={handleSubmit}>
            <Item>
              {getFieldDecorator('email', {
                rules: [{
                  required: true,
                  message: 'Please input a valid email',
                }],
              })(
                <Input
                  placeholder="Email"
                  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              )}
            </Item>
            <Item>
              {getFieldDecorator('password', {
                rules: [{
                  required: true,
                  message: 'Please input a valid password',
                }],
              })(
                <Input
                  type="password"
                  placeholder="Password"
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              )}
            </Item>
            <Row type="flex" justify="end">
              <Col>
                <Button
                  type="primary"
                  htmlType="submit"
                >
                  LogIn
                </Button>
              </Col>
            </Row>
          </Form>
        </Card>
      </Col>
    </Row>
  );
}

const Login = Form.create({ name: 'new-category-form' })(LoginForm);

export default Login;
