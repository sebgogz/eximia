import React from 'react';
import { Card, Table, Row, Col, Button } from 'antd';

import { useGetUsers } from '../../apollo/hooks';
import HandleResponse from '../../apollo/components/HandleResponse';

import urlService from '../../services/urlService';
import useHistory from '../../common/hooks/useHistory';

import routes from '../../routes';

import { columns } from './constants';

import './Users.scss';

const { register, buySide } = routes;

function Users() {
  const history = useHistory();
  const { data, loading, error } = useGetUsers({});

  const users = data && data.getUsers;

  if (!users) {
    return (
      <HandleResponse loading={false} error={error} />
    );
  }

  function handleRegisterButton() {
    urlService.push(
      history,
      register,
    );
  }

  const handleOnRow = (record: any, rowIndex: any) => {
    return {
      onClick: () => {
        urlService.push(
          history,
          buySide,
        );
      },
    };
  }

  return (
    <Card>
      <Row
        className="actions"
        type="flex"
        justify="end"
      >
        <Col>
          <Button type="primary" onClick={handleRegisterButton}>
            Registrar Usuario
          </Button>
        </Col>
      </Row>
      <Table
        rowKey="id"
        columns={columns}
        loading={loading}
        pagination={false}
        dataSource={users || []}
        onRow={handleOnRow}
      />
    </Card>
  );
};

export default Users;
