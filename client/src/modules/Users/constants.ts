export const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
  },
  {
    title: 'Rol',
    dataIndex: 'role',
  },
  {
    title: 'Email',
    dataIndex: 'email',
  },
  {
    title: 'Creado',
    dataIndex: 'createdAt',
  },
];
