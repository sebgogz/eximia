const environments = {
  production: {
    APOLLO_SERVER_URL: 'http://localhost:3001',
  },
  staging: {
    APOLLO_SERVER_URL: 'http://localhost:3001',
  },
  development: {
    APOLLO_SERVER_URL: 'http://localhost:3001',
  },
};

export default environments[process.env.MODE] || environments.development;
