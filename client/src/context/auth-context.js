import React, { useEffect } from 'react';
import { Row, Col, Spin } from 'antd';
import { useLogin, useLazyGetUser, useRegister, GET_USERS } from '../apollo/hooks';

const AuthContext = React.createContext();

function onUserRegister(cache, { data: { register } }) {
  const { getUsers } = cache.readQuery({ query: GET_USERS });
  cache.writeQuery({
    query: GET_USERS,
    data: {
      getUsers: getUsers.concat([register]),
    },
  });
}

function AuthProvider(props) {
  const [ getUser, {
    data: userResponse,
    loading: userLoading,
  }] = useLazyGetUser({});
  const [ doLogin, {
    data: loginResponse,
    loading: loginLoading,
    client,
  }] = useLogin({});
  const [ doRegister, {
    data: registerResponse,
    loading: registerLoading,
    error: registerError,
  }] = useRegister({
    update: onUserRegister
  });

  const saveToken = (token) => {
    localStorage.setItem('token', token);
  };

  const removeToken = () => {
    localStorage.removeItem('token');
  }

  const token = localStorage.getItem('token');
  const user = userResponse && userResponse.getUser;
  const userToken = loginResponse && loginResponse.login && loginResponse.login.token;

  const login = (params) => {
    doLogin({ variables: { params } });
  };
  const register = (params) => {
    doRegister({ variables: { params } });
  };
  const logout = () => {
    removeToken();
    client.resetStore();
  };

  useEffect(() => {
    getUser();
  }, [ token, getUser ]);

  useEffect(() => {
    if (userToken) {
      saveToken(userToken);
    }
    getUser();
  }, [ userToken, getUser ]);

  if (loginLoading || userLoading) {
    return (
      <Row className="login-spiner" type="flex" justify="center" align="middle">
        <Col>
          <Spin size="large" />
        </Col>
      </Row>
    );
  }

  const authData = {
    user,
    login,
    logout,
    register,
    registerResponse,
    registerLoading,
    registerError,
  };

  return (
    <AuthContext.Provider value={authData} {...props} />
  );
}

const useAuth = () => React.useContext(AuthContext);

export { AuthProvider, useAuth };
