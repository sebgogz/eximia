export type RouteType = {
  name: string,
  path: string,
};

export type RoutesType = { [name: string]: RouteType };
