import { RoutesType } from './types';

const routes: RoutesType = {
  buySide: {
    name: 'buySide',
    path: '/',
  },
  users: {
    name: 'users',
    path: '/users',
  },
  register: {
    name: 'register',
    path: '/register',
  },
};

export default routes;
