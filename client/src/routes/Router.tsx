import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import routes from '.';

import Users from '../modules/Users';
import Register from '../modules/Register';
import BuySide from '../modules/BuySide';

function Router() {
  return (
    <Switch>
      <Route
        exact
        path={routes.buySide.path}
        component={BuySide}
      />
      <Route
        exact
        path={routes.users.path}
        component={Users}
      />
      <Route
        exact
        path={routes.register.path}
        component={Register}
      />
      <Redirect to={routes.buySide.path} />
    </Switch>
  );
}

export default Router;
