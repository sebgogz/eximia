import dotObject from 'dot-object';
import querystring from 'querystring';
import { pathToRegexp } from 'path-to-regexp';

class UrlService {
  stringify = (givenObject) => {
    const flattenedObject = dotObject.dot(givenObject);
    return querystring.stringify(flattenedObject);
  }

  parse = (search) => {
    const stringifiedFlattenedObject = querystring.parse(search.replace(/^\?/, ''));
    return dotObject.object(stringifiedFlattenedObject);
  }

  to = (to, options) => {
    let path;
    const { params, query } = options || {};

    if (params) {
      const toPath = pathToRegexp.compile(to.path);
      path = toPath(params);
    } else {
      ({ path } = to);
    }

    return path
      .replace('(.*)', '')
      .concat(
        query ? '?'.concat(this.stringify((query))) : '',
      );
  }

  push = (history, to, options) => {
    const pushTo = this.to(to, options);
    history.push(pushTo);
  }
}

export default new UrlService();
