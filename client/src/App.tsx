import React from 'react';
import { BrowserRouter as BrowserRouterProvider } from 'react-router-dom';

import { useUser } from './context/user-context';

import Router from './routes/Router';

import Login from './modules/Login';

import Layout from './common/components/Layout';

const App: React.FC = () => {
  const user = useUser();

  return (
    <BrowserRouterProvider>
      <Layout>
        {user ? <Router /> : <Login />}
      </Layout>
    </BrowserRouterProvider>
  );
}

export default App;
