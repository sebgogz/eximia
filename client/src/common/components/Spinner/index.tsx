import React from 'react';

import { Spin } from 'antd';

type Props = {
  loading: boolean,
  size: 'small' | 'default' | 'large',
};

const Spinner = (props: Props) => {
  const { loading, ...remainSpinProps } = props;

  if (!loading) {
    return null;
  }

  return (
    <Spin className="margin-0-auto" {...remainSpinProps} />
  );
};

export default Spinner;
