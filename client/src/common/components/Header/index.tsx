import React from 'react';
import { Avatar, Layout, Menu, Dropdown, Button, Row, Col } from 'antd';

import { useUser } from '../../../context/user-context';
import { useAuth } from '../../../context/auth-context';

import routes from '../../../routes';

import Link from '../Link';

import './Header.scss';

const { buySide, users } = routes;
const { Item } = Menu;
const { Header } = Layout;

type MenuItem = {
  key: string,
};

function HeaderComponent() {
  const auth = useAuth();
  const user = useUser();
  const { logout } = auth;
  const userEmail = user && user.email;

  function onMenuClick({ key }: MenuItem) {
    switch(key) {
      case 'logout':
        logout();
        break;
      default:
        return;
    }
  }

  const userMenu = (
    <Menu
      onClick={onMenuClick}
    >
      <Item key="logout">
        Cerrar Sesión
      </Item>
    </Menu>
  );

  return (
    <Header className="header">
      <Row className="row-header" type="flex" justify="space-between">
        <Col>
          <Row className="row-header" type="flex" gutter={32}>
            <Col>
              <div>
                <Link to={buySide}>
                  <span className="header-title">Eximia</span>
                </Link>
              </div>
            </Col>
            <Col>
              {user && (
                <Menu
                  theme="dark"
                  mode="horizontal"
                  className="header-menu"
                >
                  <Item>
                    <Link to={users}>
                      Usuarios
                    </Link>
                  </Item>
                </Menu>
              )}
            </Col>
          </Row>
        </Col>
        <Col>
          {user && (
            <Menu
              theme="dark"
              mode="horizontal"
              selectable={false}
              className="header-menu"
            >
              <Item className="header-user">
                <Dropdown overlay={userMenu} trigger={['click']}>
                  <Button className="user-menu-btn" type="link">
                    <Avatar size="small" icon="user" />
                    <span className="user-label">{userEmail || 'User'}</span>
                  </Button>
                </Dropdown>
              </Item>
            </Menu>
          )}
        </Col>
      </Row>
    </Header>
  );
}

export default HeaderComponent;
