import React from 'react';
import { Link as ReactRouterLink } from 'react-router-dom';

import urlService from '../../../services/urlService';
import { RouteType } from '../../../routes/types';

export type LinkProps = {
  to: RouteType,
  query?: Object,
  params?: Object,
};

const Link: React.FC<LinkProps> = ({
  to,
  query,
  params,
  children,
  ...remainingProps
}) => {
  const linkProps = {
    to: urlService.to(to, { query, params }),
    ...remainingProps,
  };

  return (
    <ReactRouterLink {...linkProps}>{children}</ReactRouterLink>
  );
}

export default Link;
