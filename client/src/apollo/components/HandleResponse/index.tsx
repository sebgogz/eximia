import React from 'react';

// common
import Spinner from '../../../common/components/Spinner';
import Error from '../Error';

type Props = {
  error: any,
  loading: boolean
};

function HandleResponse(props: Props) {
  const { error, loading } = props;

  if (error) {
    return <Error error={error}/>;
  }
  return <Spinner loading={loading} size="large" />;
}

export default HandleResponse;
