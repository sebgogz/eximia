import React from 'react';
import { Alert } from 'antd';

type Props = {
  error: any
};

function Error(props: Props) {
  const { graphQLErrors, networkError } = props.error;

  if (!graphQLErrors && !networkError) {
    return null;
  }

  let title = 'GraphQL Errors';
  const errorArray = [...graphQLErrors];
  if (networkError) {
    const { result } = networkError;
    errorArray.push(...(result ? result.errors : []));
    title = 'Network Error';
  }
  const description = errorArray.map(val => <span key={`${val.type || ''}${val.message}`}> {val.message} </span>);

  return (
    <Alert
      className="margin-0-auto"
      message={title}
      description={description}
      type="error"
      showIcon
    />
  );
}

export default Error;
