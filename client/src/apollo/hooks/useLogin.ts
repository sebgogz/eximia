import { useMutation, MutationHookOptions, } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

export const LOGIN = gql`
  mutation login($params: LoginParams!) {
    login(params: $params) {
      auth
      token
      message
    }
  }
`;

export default (options: MutationHookOptions) => useMutation(LOGIN, options);
