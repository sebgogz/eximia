import { useQuery, QueryHookOptions } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

export const GET_USERS = gql`
  query getUsers {
    getUsers {
      id
      role
      email
      createdAt
      updatedAt
    }
  }
`;

export default (options: QueryHookOptions) => useQuery(GET_USERS, options);
