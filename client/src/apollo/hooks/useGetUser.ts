import { useQuery, useLazyQuery, QueryHookOptions, LazyQueryHookOptions } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

export const GET_USER = gql`
  query getUser {
    getUser {
      id
      email
      role
      createdAt
      updatedAt
    }
  }
`;

export const useLazyGetUser = (options: LazyQueryHookOptions) => useLazyQuery(GET_USER, options);
export default (options: QueryHookOptions) => useQuery(GET_USER, options);
