import { useMutation, MutationHookOptions, } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

export const REGISTER = gql`
  mutation register($params: RegisterParams!) {
    register(params: $params) {
      id
      role
      email
      createdAt
      updatedAt
    }
  }
`;

export default (options: MutationHookOptions) => useMutation(REGISTER, options);
