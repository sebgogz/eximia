import { useQuery, QueryHookOptions } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

export const GET_ACCOUNTS = gql`
  query getAccounts {
    getAccounts {
      id
      clientId
      clientName
      clientLastN
      clientMail
      clientPhone
      accountInfo {
        accountId
        balance
        rend
        strategy {
          result {
            index
            columns
            data
          }
          primeResult
          deltaResult
          df {
            index
            columns
            data
          }
          prime
          status
          delta
          gamma
          theta
          vega
          life
          type
          strike
          amount
        }
        book {
          oid
          underlying
          position
          type
          strike
          term
          amount
          prime
          delta
          gamma
          theta
          vega
          life
          cp
          date {
            date
          }
          expDate {
            date
          }
        }
      }
    }
  }
`;

export default (options: QueryHookOptions) => useQuery(GET_ACCOUNTS, options);
