
from setuptools import setup



setup(name='manager',

      version='1',

      description='An account manager',

      url='https://github.com/agui197/pyManager',

      author='agui197',

      author_email='if702319@iteso.mx',

      license='GPL',

      packages=['manager'],

      zip_safe=False)