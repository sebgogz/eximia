
from setuptools import setup



setup(name='options',

      version='1.2',

      description='A options designer',

      url='https://github.com/agui197/pyOptions',

      author='agui197',

      author_email='if702319@iteso.mx',

      license='GPL',
      
      install_requires=['pandas_datareader','oandapyV20','plotly','chart-studio','mibian','matlab','pymongo'],

      packages=['options'],

      zip_safe=False)