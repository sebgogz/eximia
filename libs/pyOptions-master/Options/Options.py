import pandas as pd
import numpy as np
from copy import deepcopy
import pandas_datareader.data as web
from oandapyV20 import API
import oandapyV20.endpoints.instruments as instruments
import datetime
import scipy.stats as st
import matplotlib.pyplot as plt
import time

apikey='EPlOOyMfwBSpewaxJOF9'
username='agui197'

import pymongo
#con = "mongodb://automaticForexScript:lkj031j3r1-.ajs.13r1@134.209.74.56:27017/"
#client = pymongo.MongoClient(con)
#collection = client.currencies
#table = collection.investing

con = "mongodb://bloombergReporting:G9JWSR8828Aq4HmfbkvP@134.209.74.56:27017/"
client = pymongo.MongoClient(con)
collection=client.bloomberg
bloombergData=collection.instruments

#TODO:
    #modification in vanilla_pricing (details there) use midmarket 
    #modification in all functions that receive a dataframe change it to dict
    
    #confirmation butterfly_opt isn't affected by change of historicVol
###################################################### Volatility #################################################################

def volDx(ATM=None,RRD=None,BFD=None,x=None):
    ''' VolDx calculates the volatility value for a term for every delta from (0-1) using ATM,RR and BF volatility values
    
        Parameters
        ----------
        ATM: float
        RRD: float
        BFD: float
        x  : float
    
    '''
    return ATM+2*RRD*(x-.5)+16*BFD*(x-.5)**2

def smile_vols(vols=None,periods=[28,60,90,250]):
    ''' smile_vols Complete interpolation (volatility values) for all terms for deltas (0-1) 
    
        Parameters
        ----------
        vols    : Dataframe (this can be change to fit an array)
        periods : array
    
    '''
    smiles=list(map(lambda period:list(map(lambda delta:volDx(vols.loc[period].ATM,vols.loc[period].RR25D,vols.loc[period].BF25D,delta),list(np.arange(0,1,.05)))),vols.transpose()))
    result=pd.DataFrame(smiles,index=periods,columns=list(np.arange(0.00000001,1,.05)))
    return result

def strike_fun(delta=None,spot=None,domesticRate=None,foreignRate=None,days_maturity=None,volatility=None,yer_periodicity=None):
    ''' strike_fun finds the corresponding strike for a certain value of delta
    
        Parameters
        ----------
        delta           : float
        spot            : float
        domesticRate    : float
        foreignRate     : float
        days_maturity   : float
        volatility      : float
        yer_periodicity : int 
    
    '''
    result=spot/np.exp(st.norm.ppf(np.exp(foreignRate*days_maturity/yer_periodicity)*delta)*volatility*np.sqrt(days_maturity/yer_periodicity)-(domesticRate-foreignRate+volatility**2/2)*days_maturity/yer_periodicity)
    return result

def global_vol(conection=1,underlying='USD_MXN',collection=None):
    ''' global_vol extracts volatilty values from investing or bloomberg for one underlying 
    
        Parameters
        ----------
        connection  : int
        underlying  : str 
        table       : dict
    
    '''
    #correct here make the table element a tuple in wich depending on the type of conection it selects the table for the calculations
    #this selection may  be better done depending on wich  underlying we want to calculate
    #if is one existent in the data provided by bloomberg it should choose bloomberg as source
    #if not it should grab investing table
    ############################### bloomberg or investing ###############################
#    if(conection==1):
#        lastvol=table.find()[table.estimated_document_count()-1]
#        ############################### underlying ###############################
#        if(underlying=='USD_MXN'):
#            result=lastvol['tables'][15]
    #else:
        #bloomberg extraction
        
    ############################### bloomberg or investing ###############################
    if(conection==1):
        ############################### underlying ###############################
        if(underlying=='USD_MXN'):
            idIntrument=['IX2061168-0',
             'IX755090-0',
             'IX1872670-0',
             'IX2365172-0',
             'IX411947-0',
             'IX411949-0',
             'IX411951-0',
             'IX2365300-0',
             'IX411953-0',
             'IX411955-0',
             'IX411957-0',
             'IX2661310-0',
             'IX755092-0',
             'IX2062720-0',
             'IX763875-0',
             'IX2365136-0',
             'IX2365180-0',
             'IX2557814-0',
             'IX763877-0',
             'IX763879-0',
             'IX2365308-0',
             'IX763881-0',
             'IX1039653-0',
             'IX763883-0',
             'IX1039657-0',
             'IX763885-0',
             'IX2248389-0',
             'IX763953-0',
             'IX2365156-0',
             'IX2365200-0',
             'IX763955-0',
             'IX763957-0',
             'IX763959-0',
             'IX2365328-0',
             'IX763961-0',
             'IX1039645-0',
             'IX763963-0',
             'IX2662881-0',
             'IX763965-0']

    result=list(map(lambda instrument:list(collection.aggregate([
    {'$match':{'instruments.idBBUnique':instrument}},
    {'$project': {
          'metadata': {
            '$filter': {
                'input': "$instruments",
                'as': "instrument",
                'cond': { '$or': [
                    {'$eq': [ "$$instrument.idBBUnique",instrument]}]}
              }
            }
          }
    }
    ]))[-1],idIntrument))
    
    return result


def strike_smile_tables(historicVol=None,foreignRate=None,domesticRate=None,spot=None):
    ''' strike_smile_tables builds the vol surface and their corresponding strike values
    
        Parameters
        ----------
        historicVol  : dict
        domesticRate : float
        foreignRate  : float
        spot         : float
    
    '''
    starting_point = time.time()
    

    investing_periods=[1,7,14,21,28,60,90,120,180,270,360,540,720]
    askVols=list(map(lambda value:float(value['metadata'][0]['pxAsk'])/100,historicVol))
    askVols=np.array(askVols).reshape((3,13))
    askVols=pd.DataFrame(askVols.T,index=investing_periods,columns=['ATM','RR25D','BF25D'])

    bidVols=list(map(lambda value:float(value['metadata'][0]['pxBid'])/100,historicVol))
    bidVols=np.array(bidVols).reshape((3,13))
    bidVols=pd.DataFrame(bidVols.T,index=investing_periods,columns=['ATM','RR25D','BF25D'])
    
    asksmile=np.abs(smile_vols(vols=askVols,periods=investing_periods))
    askcallStrikes=list(map(lambda period:list(map(lambda delta:strike_fun(delta=delta,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=period,volatility=asksmile.loc[period][delta],yer_periodicity=360),asksmile.columns)),asksmile.transpose()))
    askcallStrikes=pd.DataFrame(askcallStrikes,index=investing_periods,columns=list(np.arange(0.00000001,1,.05)))
    askcallStrikes.index.name='askcallStrikes'
    
    askputStrikes=list(map(lambda period:list(map(lambda delta:strike_fun(delta=1-delta,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=period,volatility=asksmile.loc[period][delta],yer_periodicity=360),asksmile.columns)),asksmile.transpose()))
    askputStrikes=pd.DataFrame(askputStrikes,index=investing_periods,columns=list(np.arange(0.00000001,1,.05)))
    askputStrikes.index.name='askputStrikes'
    
    bidsmile=smile_vols(vols=bidVols,periods=investing_periods)
    bidcallStrikes=list(map(lambda period:list(map(lambda delta:strike_fun(delta=delta,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=period,volatility=bidsmile.loc[period][delta],yer_periodicity=360),bidsmile.columns)),bidsmile.transpose()))
    bidcallStrikes=pd.DataFrame(bidcallStrikes,index=investing_periods,columns=list(np.arange(0.00000001,1,.05)))
    bidcallStrikes.index.name='bidcallStrikes'
    
    bidputStrikes=list(map(lambda period:list(map(lambda delta:strike_fun(delta=1-delta,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=period,volatility=bidsmile.loc[period][delta],yer_periodicity=360),bidsmile.columns)),bidsmile.transpose()))
    bidputStrikes=pd.DataFrame(bidputStrikes,index=investing_periods,columns=list(np.arange(0.00000001,1,.05)))
    bidputStrikes.index.name='bidputStrikes'
    #smile=np.abs(smile)+.05
    elapsed_time = time.time () - starting_point
    elapsed_time_int = int(elapsed_time) 
    return askcallStrikes,askputStrikes,bidcallStrikes,bidputStrikes,asksmile,bidsmile,elapsed_time_int


######################################################## Yields #################################################################
def yield_interpolation(yieldCurve=None,Term=None):
    ''' Interpolates yield values for a non-existent value in the yield curve
    
        Parameters
        ----------
        yieldCurve  : dict
        Term        : int 
    
    '''
    yieldCurve=pd.DataFrame(list(map(lambda value:float(value['metadata'][0]['pxLast'])/100,yieldCurve)))
    if(len(yieldCurve)==5):
        yieldTerms=np.array([28,90,180,360,720])
    elif(len(yieldCurve)==3):
        yieldTerms=np.array([28,91,182])        
    try:
        YM=yieldCurve.iloc[np.where(yieldTerms>=Term)[0][0],0]
    except:
        return yieldCurve.iloc[-1,0]
    try:
        Ym=yieldCurve.iloc[np.where(yieldTerms<Term)[0][-1],0]
    except:
        return yieldCurve.iloc[0,0]
    DM=yieldTerms[np.where(yieldTerms>=Term)[0][0]]
    Dm=yieldTerms[np.where(yieldTerms<Term)[0][-1]]
    return (YM-Ym)/(DM-Dm)*(Term-Dm)+Ym


####################################################### Pricing #################################################################

def BlsPrice(spot=None,strike=None,domesticRate=None,foreignRate=None,days_maturity=None,volatility=[None,None],yer_periodicity=None):
    ''' BlsPrice BlackScholes valuation for a exchange-rate derivative
    
        Parameters
        ----------
        spot            : float
        strike          : float
        domesticRate    : float
        foreignRate     : float
        days_maturity   : float
        volatility      : float
        yer_periodicity : int 
        
    '''
    d1=np.log(spot/strike)+(domesticRate-foreignRate+volatility[0]**2/2)*days_maturity/yer_periodicity
    d1=d1/(volatility[0]*np.sqrt(days_maturity/yer_periodicity))
    d2=d1-(volatility[0]*np.sqrt(days_maturity/yer_periodicity))
    primaCall=spot*np.exp(-foreignRate*days_maturity/yer_periodicity)*st.norm(0,1).cdf(d1)-strike*np.exp(-domesticRate*days_maturity/yer_periodicity)*st.norm(0,1).cdf(d2)
    d1=np.log(spot/strike)+(domesticRate-foreignRate+volatility[1]**2/2)*days_maturity/yer_periodicity
    d1=d1/(volatility[1]*np.sqrt(days_maturity/yer_periodicity))
    d2=d1-(volatility[1]*np.sqrt(days_maturity/yer_periodicity))
    primaPut=strike*np.exp(-domesticRate*days_maturity/yer_periodicity)*st.norm(0,1).cdf(-d2)-spot*np.exp(-foreignRate*days_maturity/yer_periodicity)*st.norm(0,1).cdf(-d1)
    result=[primaCall,primaPut]
    return result

def BlsDelta(spot=None,strike=None,domesticRate=None,foreignRate=None,days_maturity=None,volatility=[None,None],yer_periodicity=None):
    ''' BlsDelta BlackScholes delta for a exchange-rate derivative
    
        Parameters
        ----------
        spot            : float
        strike          : float
        domesticRate    : float
        foreignRate     : float
        days_maturity   : float
        volatility      : float
        yer_periodicity : int 
    
    '''
    d1=np.log(spot/strike)+(domesticRate-foreignRate+volatility[0]**2/2)*days_maturity/yer_periodicity
    d1=d1/(volatility[0]*np.sqrt(days_maturity/yer_periodicity))
    deltaCall=np.exp(-foreignRate*days_maturity/yer_periodicity)*st.norm(0,1).cdf(d1)
    d1=np.log(spot/strike)+(domesticRate-foreignRate+volatility[1]**2/2)*days_maturity/yer_periodicity
    d1=d1/(volatility[1]*np.sqrt(days_maturity/yer_periodicity))
    deltaPut=-np.exp(-foreignRate*days_maturity/yer_periodicity)*st.norm(0,1).cdf(-d1)
    result=[deltaCall,np.abs(deltaPut)]
    return result

def BlsGamma(spot=None,strike=None,domesticRate=None,foreignRate=None,days_maturity=None,volatility=None,yer_periodicity=None):
    ''' BlsGamma BlackScholes gamma for a exchange-rate derivative
    
        Parameters
        ----------
        spot            : float
        strike          : float
        domesticRate    : float
        foreignRate     : float
        days_maturity   : float
        volatility      : float
        yer_periodicity : int 
    
    '''
    d1=np.log(spot/strike)+(domesticRate-foreignRate+volatility**2/2)*days_maturity/yer_periodicity
    d1=d1/(volatility*np.sqrt(days_maturity/yer_periodicity))
    gamma=st.norm(0,1).pdf(d1)*np.exp(-foreignRate*days_maturity/yer_periodicity)/(spot*volatility*np.sqrt(days_maturity/yer_periodicity))
    result=gamma
    return result

def BlsTheta(spot=None,strike=None,domesticRate=None,foreignRate=None,days_maturity=None,volatility=[None,None],yer_periodicity=None):
    ''' BlsTheta BlackScholes Theta for a exchange-rate derivative
    
        Parameters
        ----------
        spot            : float
        strike          : float
        domesticRate    : float
        foreignRate     : float
        days_maturity   : float
        volatility      : float
        yer_periodicity : int 
    
    '''
    d1=np.log(spot/strike)+(domesticRate-foreignRate+volatility[0]**2/2)*days_maturity/yer_periodicity
    d1=d1/(volatility[0]*np.sqrt(days_maturity/yer_periodicity))
    d2=d1-(volatility[0]*np.sqrt(days_maturity/yer_periodicity))
    thetaCall=-spot*st.norm(0,1).pdf(d1)*volatility[0]*np.exp(-foreignRate*days_maturity/yer_periodicity)/(2*np.sqrt(days_maturity/yer_periodicity))+foreignRate*spot*st.norm(0,1).cdf(d1)*np.exp(-foreignRate*days_maturity/yer_periodicity)-domesticRate*strike*np.exp(-domesticRate*days_maturity/yer_periodicity)*st.norm(0,1).cdf(d2)
    d1=np.log(spot/strike)+(domesticRate-foreignRate+volatility[1]**2/2)*days_maturity/yer_periodicity
    d1=d1/(volatility[1]*np.sqrt(days_maturity/yer_periodicity))
    d2=d1-(volatility[1]*np.sqrt(days_maturity/yer_periodicity))
    thetaPut=-spot*st.norm(0,1).pdf(d1)*volatility[1]*np.exp(-foreignRate*days_maturity/yer_periodicity)/(2*np.sqrt(days_maturity/yer_periodicity))-foreignRate*spot*st.norm(0,1).cdf(-d1)*np.exp(-foreignRate*days_maturity/yer_periodicity)+domesticRate*strike*np.exp(-domesticRate*days_maturity/yer_periodicity)*st.norm(0,1).cdf(-d2)
    result=[np.abs(thetaCall),np.abs(thetaPut)]
    return result

def BlsVega(spot=None,strike=None,domesticRate=None,foreignRate=None,days_maturity=None,volatility=None,yer_periodicity=None):
    ''' BlsVega BlackScholes Vega for a exchange-rate derivative
    
        Parameters
        ----------
        spot            : float
        strike          : float
        domesticRate    : float
        foreignRate     : float
        days_maturity   : float
        volatility      : float
        yer_periodicity : int 
    
    '''
    d1=np.log(spot/strike)+(domesticRate-foreignRate+volatility**2/2)*days_maturity/yer_periodicity
    d1=d1/(volatility*np.sqrt(days_maturity/yer_periodicity))
    vega=spot*np.sqrt(days_maturity/yer_periodicity)*np.exp(-foreignRate*days_maturity/yer_periodicity)*st.norm(0,1).pdf(d1)
    result=vega
    return result

def vanilla_pricing(historicVol=None,spot=None,strike=None,position=None,domesticRate=None,foreignRate=None,days_maturity=None,volatility=None,yer_periodicity=360,disable=1):
    ''' Vanilla_pricing Calculation for all greeks and price of vanilla type options
    
        Parameters
        ----------
        historicVol     : dict
        spot            : float
        strike          : float
        domesticRate    : dict
        foreignRate     : dict
        days_maturity   : float
        volatility      : float
        yer_periodicity : int 
        disable         : int (boolean)
    
    '''
    if disable==0:
        
        ################################## yield selection ######################################
        domesticRate=yield_interpolation(yieldCurve=domesticRate,Term=days_maturity)
        foreignRate=yield_interpolation(yieldCurve=foreignRate,Term=days_maturity)
        
        ################################## volatility selection ##################################
        #volatility for gamma and vega must chose from call or put vols
        #change function for be for optimal
        
        
        askcallStrikes,askputStrikes,bidcallStrikes,bidputStrikes,asksmile,bidsmile,elapsed_time_int=strike_smile_tables(historicVol=historicVol,foreignRate=foreignRate,domesticRate=domesticRate,spot=spot)
        
        if position==1:
            callvol=asksmile.iloc[pd.Series(np.abs(askcallStrikes.index-days_maturity)).idxmin()][pd.Series(np.abs(askcallStrikes.iloc[pd.Series(np.abs(askcallStrikes.index-days_maturity)).idxmin()]-strike)).idxmin()]+volatility
            putvol=asksmile.iloc[pd.Series(np.abs(askputStrikes.index-days_maturity)).idxmin()][pd.Series(np.abs(askputStrikes.iloc[pd.Series(np.abs(askputStrikes.index-days_maturity)).idxmin()]-strike)).idxmin()]+volatility
        else:
            callvol=bidsmile.iloc[pd.Series(np.abs(bidcallStrikes.index-days_maturity)).idxmin()][pd.Series(np.abs(bidcallStrikes.iloc[pd.Series(np.abs(bidcallStrikes.index-days_maturity)).idxmin()]-strike)).idxmin()]-volatility
            putvol=bidsmile.iloc[pd.Series(np.abs(bidputStrikes.index-days_maturity)).idxmin()][pd.Series(np.abs(bidputStrikes.iloc[pd.Series(np.abs(bidputStrikes.index-days_maturity)).idxmin()]-strike)).idxmin()]-volatility
            
            
        #################################### PRICING #############################################
        result=BlsPrice(spot=spot,strike=strike,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=[callvol,putvol],yer_periodicity=yer_periodicity)
        
        ################################## GREEKS #############################################
        delta=BlsDelta(spot=spot,strike=strike,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=[callvol,putvol],yer_periodicity=yer_periodicity)
        #volatility correction
        gamma=BlsGamma(spot=spot,strike=strike,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=callvol,yer_periodicity=yer_periodicity)
        #volatility correction
        theta=BlsTheta(spot=spot,strike=strike,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=[callvol,putvol],yer_periodicity=yer_periodicity)
        vega=BlsVega(spot=spot,strike=strike,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=callvol,yer_periodicity=yer_periodicity)
        
    else:
        result=[0,0]
        delta=[0,0]
        gamma=0
        theta=[0,0]
        vega=0
    return result,delta,gamma,theta,vega

####################################################### Building Blocks #################################################################
def grapher(result=None,name=None,min_val_graph=15,max_val_graph=26):
    ''' grapher function draws the P&L graph for an option 
    
        Parameters
        ----------
        result        : array
        name          : str
        min_val_graph : int
        max_val_graph : int
    
    '''
    yaxis=np.arange(min_val_graph,max_val_graph,.01)    
    fig=plt.figure(figsize=(8,8))
    ax=fig.add_subplot(111)
    ax.clear()
    ax.plot(yaxis,result)
    plt.title('Graph P&L')
    plt.legend([name])
    plt.xlabel('Dolar')
    plt.ylabel('Profit')

def call_vanilla(strike=None,min_val_graph=15,max_val_graph=26,position=1,amount=1,prime=0):
    ''' call_vanilla Basic contruction block a call vanilla option, returns a P&L array for the option
    
        Parameters
        ----------
        strike        : float
        min_val_graph : int
        max_val_graph : int
        position      : int
        amount        : int
        prime         : float
    
    '''
    yaxis=np.arange(min_val_graph,max_val_graph,.01)
    result=np.arange(min_val_graph,max_val_graph,.01)
    result[yaxis<=strike]=0
    result[yaxis>strike]=(yaxis[yaxis>strike]-strike)*position
    return (result-prime*position)*amount

def put_vanilla(strike=None,min_val_graph=15,max_val_graph=26,position=1,amount=1,prime=0):
    ''' put_vanilla Basic contruction block a put vanilla option, returns a P&L array for the option
    
        Parameters
        ----------
        strike        : float
        min_val_graph : int
        max_val_graph : int
        position      : int
        amount        : int
        prime         : float
    
    '''
    yaxis=np.arange(min_val_graph,max_val_graph,.01)
    result=np.arange(min_val_graph,max_val_graph,.01)
    result[yaxis>=strike]=0
    result[yaxis<strike]=(strike-yaxis[yaxis<strike])*position
    return (result-prime*position)*amount

####################################################### Opcionador #################################################################

#### Conocimiento de todas los tipos de opciones para valuar y registrar la informacion para el tipo de opcion deseado
def validation(strike=None,position=None,amount=None):
    ''' validation function that prevents mising information for vanilla pricing
    
        Parameters
        ----------
        strike        : float
        position      : int
        amount        : int
    
    '''
    cant_continue=True
    while(cant_continue):
        while(position==None):
            position=int(input('whats your position long/short "1/-1"'))
            if(position!=1):
                if(position!=-1):
                    position=None
        

        while(strike==None):
            strike=float(input('whats your strike n>0 '))
            if(strike<=0):
                strike=None
        while(amount==None):
            amount=int(input('whats your amount n>0 '))
            if(amount<=0):
                amount=None       
        cant_continue=False
    return strike,position,amount

def flatten(iterable):
    ''' Flatten converts nested lists to one-dimension arrays
    
        Parameters
        ----------
        iterable : array
    
    '''
    try:
        for item in iterable:
            yield from flatten(item)
    except TypeError:
        yield iterable

def get_spot(underlying="USD_MXN"):
    ''' get_spot extracts the current spot of an exchange-rate
    
        Parameters
        ----------
        underlying : str
    
    '''
    A1_OA_Da = 16                     # Day Align
    A1_OA_Ta = "America/Mexico_City"  # Time Align
    A1_OA_In = underlying              # Instrumento
    A1_OA_Gn = "M1"                   # Granularidad de velas
    A1_OA_Ak = "9a6c2c799e33de6f675b42d1dae00176-7f3debfca7f44c2f0a35fb0e15a22bd1"
    ##obtener el spot
    today = datetime.datetime.now()
    DD = datetime.timedelta(minutes=3000)
    earlier = today - DD
    F1=today.strftime("%Y-%m-%dT%H:%M:%SZ")
    F2=earlier.strftime("%Y-%m-%dT%H:%M:%SZ")
    api = API(access_token=A1_OA_Ak)
    params = {"granularity": A1_OA_Gn, "price": "M", "dailyAlignment": A1_OA_Da,
              "alignmentTimezone": A1_OA_Ta, "from": F2, "to": F1}
    A1_Req1 = instruments.InstrumentsCandles(instrument=A1_OA_In, params=params)
    A1_Hist = api.request(A1_Req1)
    a=list(map(lambda i:[A1_Hist['candles'][i]['time'],float(A1_Hist['candles'][i]['mid']['o']),float(A1_Hist['candles'][i]['mid']['h']),
                          float(A1_Hist['candles'][i]['mid']['l']),float(A1_Hist['candles'][i]['mid']['c'])],range(len(A1_Hist['candles']))))
    pd_hist = pd.DataFrame(a,columns=['TimeStamp','Open','High','Low','Close'])
    pd_hist = pd_hist[['TimeStamp', 'Open', 'High', 'Low', 'Close']]
    pd_hist['TimeStamp'] = pd.to_datetime(pd_hist['TimeStamp'])
    spot=pd_hist.iloc[-1]
    return spot 

def days_to_maturitys(days_maturity=None,revisions=None):
    ''' days_to_maturity returns an array of int values corresponding to the settlement avoiding
        weekends 
        
        Revisions Every day D or multiples of 7
    
        Parameters
        ----------
        days_maturity : int
        revisions     : str
    
    '''
    if revisions=='D':
        today=datetime.datetime.now()
        DD = datetime.timedelta(days=days_maturity)
        after = today + DD
        today = today + datetime.timedelta(days=1)
        dates=pd.date_range(start=today,end=after)
        days=dates[[i for i in np.arange(len(dates)) if dates[i].weekday() not in (5, 6)]]-(today-datetime.timedelta(days=1))
        result=[d.days for d in days]
    else:
        result=list(np.arange(0,days_maturity+int(revisions[0:-1]),int(revisions[0:-1]))[1:])
    return result

def TC_calculation(strategy=None,budget=None,position=None):
    '''TC (tipo de cambio) actual exchange-rate for the strategy
        
        Parameters
        ----------
        strategy : dict
        budget   : float
        position : int
    
    '''
    return {'TC':pd.DataFrame(strategy['Result'].index,columns=['strategy'],index=strategy['Result'].index)+strategy['Result']/budget*position,
            'strategy':strategy}

class options:
    ''' 
    Constructor de las opciones desde sus bloques basicos
    '''
    
    def __init__(self):
        return
    
    ###################################### Merging #########################################################
    
    def join_strategy(self,elements=None):
        ''' join_strategy joins vanilla or strategy structures and transforms it into a structure strategy
        
        Parameters
        ----------
        elements : array of dicts
        
        '''
        ########## Use for combinations of strategies #########
        #In other words joining the building blocks
        df_result=elements[0]['DF']
        primes=[elements[0]['prime']]
        status=[elements[0]['status']]
        lifes=[elements[0]['life']]
        deltas=[elements[0]['delta']]
        gammas=[elements[0]['gamma']]
        thetas=[elements[0]['theta']]
        vegas=[elements[0]['vega']]
        types=[elements[0]['Type']]
        strikes=[elements[0]['Strike']]
        amounts=[elements[0]['Amount']]
        
        for element in elements[1:]:
            df_result=pd.concat([df_result,element['DF']], axis=1)
            #df_result=df_result.join(element['DF'])
            primes.append(element['prime'])
            status.append(element['status'])
            lifes.append(element['life'])
            deltas.append(element['delta'])
            gammas.append(element['gamma'])
            thetas.append(element['theta'])
            vegas.append(element['vega'])
            types.append(element['Type'])
            strikes.append(element['Strike'])
            amounts.append(element['Amount'])
            
        ########### Status validation of option ###############
        status=list(flatten(status))
        lifes=list(flatten(lifes))
        primes=list(flatten(primes))
        deltas=list(flatten(deltas))
        gammas=list(flatten(gammas))
        thetas=list(flatten(thetas))
        vegas=list(flatten(vegas))
        types=list(flatten(types))
        strikes=list(flatten(strikes))
        amounts=list(flatten(amounts))
        prime_result=sum(primes)
        delta_result=sum(deltas)
        columns=['strategy']            #Confirmation of the asignation name
        indx=list(np.where(np.array(status)>0)[0])
        result=pd.DataFrame(df_result.iloc[:,indx].sum(axis=1),columns=columns)
        ########### Registering History of blocks ############
        
        return {'Result':result,'Prime_result':prime_result,'Delta_result':delta_result,'DF':df_result,'prime':primes,'status':status,'delta':deltas,'gamma':gammas,'theta':thetas,'vega':vegas,'life':lifes,'Type':types,'Strike':strikes,'Amount':amounts}
    
    def recalculate(self,strategy=None,change_status=None,which_component=None):
        ''' recalculate changes the status of all or certain componentes in the strategy object 
            By default when change status is set to 1 it changes all status
            
            Parameters
            ----------
            strategy        : dict
            change_status   : int (boolean)
            which_component : array    
            
        '''
        working_Strategy=deepcopy(strategy)
        if change_status:
            if not which_component:
                for component in range(len(working_Strategy['status'])):
                    working_Strategy['status'][component]=(1-working_Strategy['status'][component])
            else:
                for component in which_component:
                    working_Strategy['status'][component]=(1-working_Strategy['status'][component])
        result=self.join_strategy([working_Strategy])
        return result
    
    ################################# Simple strategy options ##############################################
    
    def call_opt(self,historicVol=None,strike=None,min_val_graph=15,max_val_graph=26,position=None,prime=0,amount=None,
                 spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        ''' call_opt construction of the call option type, basic building block object
            
            Parameters
            ----------
            historicVol   : dict
            strike        : float
            min_val_graph : int
            max_val_graph : int
            position      : int
            amount        : int
            prime         : float 
            spot          : float
            domesticRate  : dict
            foreignRate   : dict
            days_maturity : int
            volatility    : float 
            disable       : int (boolean)
            
        '''
        ########## Validation to proceed ############
        strike,position,amount=validation(strike,position,amount)
        ########## Pricing of the option   ##########
        #By default near to zero
        prime,delta,gamma,theta,vega = vanilla_pricing(historicVol=historicVol,spot=spot,strike=strike,position=position,domesticRate=domesticRate,foreignRate=foreignRate ,days_maturity=days_maturity,volatility=volatility,disable=disable)
        prime=prime[0]
        ########## Calculation of greeks ############
        #greeks=calculate_greaks()
        #greeks={'delta':position*delta[0]*amount,'gamma':gamma,'theta':-1*position*theta[0]*amount,'vega':vega}
        ########## Plotting for the option ##########
        result=call_vanilla(strike=strike,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,amount=amount,prime=prime)
        #grapher(result=result,name=None,min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        ########## Registering of the Data ##########
        df=pd.DataFrame(result ,columns=['P&L '+str(position)+' call '+str(strike)],index=np.arange(min_val_graph,max_val_graph,.01))
        prime*=amount
        
        return {'DF':df,'prime':-prime*position,'status':1,'delta':position*delta[0]*amount,'gamma':gamma,'theta':-1*position*theta[0]*amount,'vega':vega,'life':days_maturity,'Type':1,'Strike':strike,'Amount':amount}
    
    def put_opt(self,historicVol=None,strike=None,min_val_graph=15,max_val_graph=26,position=None,prime=0,amount=None,
                 spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        ''' put_opt construction of the put option type, basic building block object
    
            Parameters
            ----------
            historicVol   : dict
            strike        : float
            min_val_graph : int
            max_val_graph : int
            position      : int
            amount        : int
            prime         : float 
            spot          : float
            domesticRate  : dict
            foreignRate   : dict
            days_maturity : int
            volatility    : float 
            disable       : int (boolean)
            
        '''
        ########## Validation to proceed ##########
        strike,position,amount=validation(strike,position,amount)
        ########## Pricing of the option   ##########
        #By default near to zero
        # jalar codigo de matlab para el pricing
        prime,delta,gamma,theta,vega = vanilla_pricing(historicVol=historicVol,spot=spot,strike=strike,position=position,domesticRate=domesticRate,foreignRate=foreignRate ,days_maturity=days_maturity,volatility=volatility,disable=disable)
        prime=prime[1]
        ########## Calculation of greeks ############
        #greeks=calculate_greaks()
        #greeks={'delta':-1*position*delta[1]*amount,'gamma':gamma,'theta':-1*position*theta[1]*amount,'vega':vega}
        ########## Plotting for the option ##########
        result=put_vanilla(strike=strike,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,amount=amount,prime=prime)
        #grapher(result=result,name=None,min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        ########## Registering of the Data ##########
        df=pd.DataFrame(result ,columns=['P&L '+str(position)+' put '+str(strike)],index=np.arange(min_val_graph,max_val_graph,.01))
        prime*=amount
        return {'DF':df,'prime':-prime*position,'status':1,'delta':-1*position*delta[1]*amount,'gamma':gamma,'theta':-1*position*theta[1]*amount,'vega':vega,'life':days_maturity,'Type':-1,'Strike':strike,'Amount':amount}
    
    
    
    ################################### Fist generation options ###########################################
    #Basic strategies containing vanilla options
    
    def spread_opt(self,historicVol=None,strike=[None,None],min_val_graph=15,max_val_graph=26,position=None,spread_type=1,amount=[None,None],
                 spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        '''spread_opt contruction of the spread option type, returns strategy object
            spread type 1 call 
            spread type -1 put
            
            Parameters
            ----------
            historicVol   : dict
            strike        : array of float
            min_val_graph : int
            max_val_graph : int
            position      : int
            spread_type   : int
            amount        : array of int
            spot          : float
            domesticRate  : dict
            foreignRate   : dict
            days_maturity : int
            volatility    : float 
            disable       : int (boolean)
        '''
        
        ########## Validation to proceed ##########
        
        strike1,position1,amount1=validation(strike[0],position,amount[0])
        strike2,position2,amount2=validation(strike[1],-1*position,amount[1])
        
        ########## Spread ##########
        if spread_type==1:
            opt1=self.call_opt(historicVol=historicVol,strike=strike1,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position1,amount=amount1,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt2=self.call_opt(historicVol=historicVol,strike=strike2,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position2,amount=amount2,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            if strike2>strike1:
                name='Bear call'
            else:
                name='Bull call'
        else:
            opt1=self.put_opt(historicVol=historicVol,strike=strike1,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position1,amount=amount1,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt2=self.put_opt(historicVol=historicVol,strike=strike2,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position2,amount=amount2,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            if strike2>strike1:
                name='Bull put'
            else:
                name='Bear put'

        ########## Registering of the Data ##########
        result=self.join_strategy([opt1,opt2])
        result['Result'].columns=[name +' spread']
        
        ########## Plotting for the option ##########
        #grapher(result['Result'],name=result['Result'].columns[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        return result
    
    def straddle_opt(self,historicVol=None,strike=[None,None],min_val_graph=15,max_val_graph=26,position=None,amount=[None,None],
                 spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        ''' straddle_opt contruction of the stradddle and strangle option type, returns strategy object
            2 strikes = strangle or 1 strike = straddle
            
            Parameters
            ----------
            historicVol   : dict
            strike        : array of float
            min_val_graph : int
            max_val_graph : int
            position      : int
            amount        : array of int
            prime         : float 
            spot          : float
            domesticRate  : dict
            foreignRate   : dict
            days_maturity : int
            volatility    : float 
            disable       : int (boolean)
            
        '''
        ########## Validation to proceed ##########
        if(len(strike)>1):
            strike1,position1,amount1=validation(strike[0],position,amount[0])
            strike2,position2,amount2=validation(strike[1],position1,amount[1])
            opt_type=' strangle'
        else:
            strike1,position1,amount1=validation(strike[0],position,amount[0])
            strike2,position2,amount2=strike1,position1,amount1
            opt_type=' straddle'
            
        ########## Straddle ##########
        if position1==1:
            opt1=self.call_opt(historicVol=historicVol,strike=strike1,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position1,amount=amount1,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt2=self.put_opt(historicVol=historicVol,strike=strike2,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position2,amount=amount2,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            name='long '
        else:
            opt1=self.call_opt(historicVol=historicVol,strike=strike1,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position1,amount=amount1,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt2=self.put_opt(historicVol=historicVol,strike=strike2,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position2,amount=amount2,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            name='short '
            
        ########## Registering of the Data ##########
        result=self.join_strategy([opt1,opt2])
        result['Result'].columns=[name + opt_type]
        
        ########## Plotting for the option ##########
        #grapher(result['Result'],name=result['Result'].columns[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        return result
    
    def butterfly_opt(self,historicVol=None,strike=None,spread=None,min_val_graph=15,max_val_graph=26,position=None,butterfly_type=None,amount=None,
                 spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        ''' butterfly_opt contruction of the butterfly option type, returns strategy object 
            Differences between types 1-3 and 2-4 are in composition but generate the same figure
            
            Parameters
            ----------
            historicVol   : dict
            strike        : float
            min_val_graph : int
            max_val_graph : int
            position      : int
            butterfly_type: int
            amount        : int
            spot          : float
            domesticRate  : dict
            foreignRate   : dict
            days_maturity : int
            volatility    : float 
            disable       : int (boolean)
            
        '''
        
        
        #Volatilitys 1.-strike, 2.- strike+spread, 3.- strike-spread
        #In this case the strike has to be the spot price
        ########## Validation to proceed ##########
        while(butterfly_type==None or butterfly_type>5):
            butterfly_type=int(input('Butterfly_type [1,4]'))
        strike,position,amount=validation(strike,position,amount)
        
        ########## Butterfly ##########
        if (butterfly_type==1):
            # long call butterfly 
            opt1=self.call_opt(historicVol=historicVol,strike=strike+spread/2,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,amount=amount,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt2=self.call_opt(historicVol=historicVol,strike=strike,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1*position,amount=2*amount,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt3=self.call_opt(historicVol=historicVol,strike=strike-spread/2,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,amount=amount,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            result=self.join_strategy([opt1,opt2,opt3])   
            name=str(position)+' call butterfly '
        elif (butterfly_type==2):
            # long put butterfly 
            opt1=self.put_opt(historicVol=historicVol,strike=strike+spread/2,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,amount=amount,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt2=self.put_opt(historicVol=historicVol,strike=strike,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1*position,amount=2*amount,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt3=self.put_opt(historicVol=historicVol,strike=strike-spread/2,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,amount=amount,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            result=self.join_strategy([opt1,opt2,opt3])   
            name=str(position)+' put butterfly '
        elif(butterfly_type==3):
            ########################## Considerar reajustar con spreads ####################################
            #iron butterfly
            opt1=self.put_opt(historicVol=historicVol,strike=strike+spread/2,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,amount=amount,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt2=self.put_opt(historicVol=historicVol,strike=strike,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1*position,amount=amount,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt3=self.call_opt(historicVol=historicVol,strike=strike,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1*position,amount=amount,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt4=self.call_opt(historicVol=historicVol,strike=strike-spread/2,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,amount=amount,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            result=self.join_strategy([opt1,opt2,opt3,opt4])
            name=str(position)+'Iron butterfly '
        elif(butterfly_type==4):
            #Alternative way for the butterfly
            strangle=self.straddle_opt(historicVol=historicVol,strike=[strike+spread/2,strike-spread/2],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1*position,amount=[amount,amount],
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            straddle=self.straddle_opt(historicVol=historicVol,strike=[strike],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,amount=[amount,amount],
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            result=self.join_strategy([strangle,straddle])
            name=str(-1*position)+' strangle '+str(position)+' straddle butterfly '
        
        ########## Registering of the Data ##########
        result['Result'].columns=[name + str(strike)+ ' ' +str(spread)]
        
        ########## Plotting for the option ##########
        #grapher(result['Result'],name=result['Result'].columns[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        return result
    
    def seagull_opt(self,historicVol=None,strike=[None,None,None],min_val_graph=15,max_val_graph=26,position=None,seagull_type=1,amount=[None,None,None],
                 spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        ''' seagull_opt contruction of the seagull option type, returns strategy object 
            seagull type 1 2 call 1 put the rule is k2>k1>k3 
            seagull type 2 2 puts 1 call the rule is k3>k1>k2
            Borders can be equal this makes a reduced seagull
            if theres's only two strikes the big one will be the same in the borders 
            
            Parameters
            ----------
            historicVol   : dict
            strike        : array of float
            min_val_graph : int
            max_val_graph : int
            position      : int
            seagull_type  : int
            amount        : array of int
            spot          : float
            domesticRate  : dict
            foreignRate   : dict
            days_maturity : int
            volatility    : float 
            disable       : int (boolean)
            
        '''
        while(seagull_type==None or seagull_type>2):
            seagull_type=int(input('seagull_type [1,2]'))
        ########## Validation to proceed ##########
        if seagull_type==1:
            #2 call 1 put
            #the rule is k2>k1>k3
            if(len(strike)>2):
                strike1,position1,amount1=validation(strike[0],position,amount[0])
                strike2,position2,amount2=validation(strike[1],position,amount[1])
                strike3,position3,amount3=validation(strike[2],-1*position,amount[2])
                while(strike2<=strike1 or strike1<=strike3):
                    strike1=float(input('k1: k2>k1>k3 '))
                    strike2=float(input('k2: k2>k1>k3 '))
                    strike3=float(input('k3: k2>k1>k3 '))
            else:
                if(strike[0]>strike[1]):
                    strike1,position1,amount1=validation(strike[0],position,amount[0])
                    strike2,position2,amount2=validation(strike1,position,amount1)
                    strike3,position3,amount3=validation(strike[1],-1*position,amount[1])
                else:
                    strike1,position1,amount1=validation(strike[0],position,amount[0])
                    strike2,position2,amount2=validation(strike[1],position,amount[1])
                    strike3,position3,amount3=validation(strike1,-1*position,amount1)
                while(strike2<strike1 or strike1<strike3):
                    strike1=float(input('k1: k2>=k1>=k3 '))
                    strike2=float(input('k2: k2>=k1>=k3 '))
                    strike3=float(input('k3: k2>=k1>=k3 '))
            spread=self.spread_opt(historicVol=historicVol,strike=[strike1,strike2],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,spread_type=1,amount=[amount1,amount2],
                                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            put=self.put_opt(historicVol=historicVol,strike=strike3,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1,amount=amount3,
                                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            result=self.join_strategy([spread,put])
            name='seagull 2 call 1 put'
        else:
            #2 puts y un call 
            #the rule is k3>k1>k2
            if(len(strike)>2):
                strike1,position1,amount1=validation(strike[0],position,amount[0])
                strike2,position2,amount2=validation(strike[1],position,amount[1])
                strike3,position3,amount3=validation(strike[2],-1*position,amount[2])
                while(strike3<=strike1 or strike1<=strike2):
                    strike1=float(input('k1: k3>k1>k2 '))
                    strike2=float(input('k2: k3>k1>k2 '))
                    strike3=float(input('k3: k3>k1>k2 '))
            else:
                if(strike[0]>strike[1]):
                    strike1,position1,amount1=validation(strike[0],position,amount[0])
                    strike2,position2,amount2=validation(strike[1],position,amount[1])
                    strike3,position3,amount3=validation(strike1,-1*position,amount1)
                else:
                    strike1,position1,amount1=validation(strike[0],position,amount[0])
                    strike2,position2,amount2=validation(strike1,position,amount1)
                    strike3,position3,amount3=validation(strike[1],-1*position,amount[1])
                while(strike3<strike1 or strike1<strike2):
                    strike1=float(input('k1: k3>=k1>=k2 '))
                    strike2=float(input('k2: k3>=k1>=k2 '))
                    strike3=float(input('k3: k3>=k1>=k2 '))
            spread=self.spread_opt(historicVol=historicVol,strike=[strike1,strike2],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,spread_type=-1,amount=[amount1,amount2],
                                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            call=self.call_opt(historicVol=historicVol,strike=strike3,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1,amount=amount3,
                                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            result=self.join_strategy([spread,call])
            name='seagull 2 put 1 call'
        ########## Registering of the Data ##########
        result['Result'].columns=[name]
        ########## Plotting for the option ##########
        #grapher(result['Result'],name=result['Result'].columns[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        return result
    
     
    def riskReversal_opt(self,historicVol=None,strike=[None,None],min_val_graph=15,max_val_graph=26,position=None,riskReversal_type=1,amount=[None,None],
                 spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        ''' riskReversal_opt contruction of the risk reversal option type, returns strategy object 
            risk resersal type 1 bullish 
            risk reversal type 2 bearish
            teorically K1>K2
            
            Parameters
            ----------
            historicVol       : dict
            strike            : array of float
            min_val_graph     : int
            max_val_graph     : int
            position          : int
            riskReversal_type : int
            amount            : int
            spot              : float
            domesticRate      : dict
            foreignRate       : dict
            days_maturity     : int
            volatility        : float 
            disable           : int (boolean)
            
        '''
        while(riskReversal_type==None or riskReversal_type>2):
            riskReversal_type=int(input('riskReversal_type [1,2]'))
        
        ########## Validation to proceed ##########
        if riskReversal_type==1:
            strike1,position1,amount1=validation(strike[0],position,amount[0])
            strike2,position2,amount2=validation(strike[1],-1*position,amount[1])
            opt1=self.call_opt(historicVol=historicVol,strike=strike1,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position1,amount=amount1,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt2=self.put_opt(historicVol=historicVol,strike=strike2,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position2,amount=amount2,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            name='bullish'
        elif riskReversal_type==2:
            strike1,position1,amount1=validation(strike[0],-1*position,amount[0])
            strike2,position2,amount2=validation(strike[1],position,amount[1])
            opt1=self.call_opt(historicVol=historicVol,strike=strike1,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position1,amount=amount1,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt2=self.put_opt(historicVol=historicVol,strike=strike2,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position2,amount=amount2,
                 spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            name='bearish'
        
        ########## Registering of the Data ##########
        result=self.join_strategy([opt1,opt2])
        result['Result'].columns=['risk reversal '+name]
        ########## Plotting for the option ##########
        #grapher(result['Result'],name=result['Result'].columns[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        return result
    
    ################################# Second generation options ###########################################
    #First generation exotics
    
    def barrier_opt(self,historicVol=None,strike=None,barrier=None,min_val_graph=15,max_val_graph=26,position=None,option_type=None,barrier_type=None,amount=None,
                     spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        ''' barrier_opt contruction of the barrier option type, returns strategy object 
            First volatility-strike,second-barrier
            option type 1 call 
            option type -1 put
           
            Barrier type 1 KI
            Barrier type -1 KO
            
            Parameters
            ----------
            historicVol       : dict
            strike            : float
            barrier           : float
            min_val_graph     : int
            max_val_graph     : int
            position          : int
            option_type       : int
            barrier_type      : int
            amount            : int
            spot              : float
            domesticRate      : dict
            foreignRate       : dict
            days_maturity     : int
            volatility        : float 
            disable           : int (boolean)
            
        '''
    
        ########## Validation to proceed ##########
        strike,position,amount=validation(strike,position,amount)
        
        ########## Barrier ##########
        if barrier_type==1:
            #RKI
            if option_type==1:
                opt1=self.digital_opt(historicVol=historicVol,strike=barrier,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,digital_type=1,payoff=np.abs(barrier-strike)*amount,
                                     spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
                opt2=self.call_opt(historicVol=historicVol,strike=barrier,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,amount=amount,
                                    spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
                name=' RKI call'
            else:
                opt1=self.digital_opt(historicVol=historicVol,strike=barrier,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,digital_type=-1,payoff=np.abs(barrier-strike)*amount,
                                     spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
                opt2=self.put_opt(historicVol=historicVol,strike=barrier,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,amount=amount,
                                    spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
                name=' RKI put'
            result=self.join_strategy([opt1,opt2])
        else:
            #RKO
            if option_type==1:
                opt1=self.digital_opt(historicVol=historicVol,strike=barrier,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1*position,digital_type=1,payoff=np.abs(barrier-strike)*amount,
                                        spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
                opt2=self.call_opt(historicVol=historicVol,strike=strike,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,amount=amount,
                                        spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
                opt3=self.call_opt(historicVol=historicVol,strike=barrier,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1*position,amount=amount,
                                        spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
                name=' RKO call'
            else:
                opt1=self.digital_opt(historicVol=historicVol,strike=barrier,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1*position,digital_type=-1,payoff=np.abs(barrier-strike)*amount,
                                        spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
                opt2=self.put_opt(historicVol=historicVol,strike=strike,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,amount=amount,
                                        spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
                opt3=self.put_opt(historicVol=historicVol,strike=barrier,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1*position,amount=amount,
                                        spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
                name=' RKO put'
            result=self.join_strategy([opt1,opt2,opt3])
        
        ########## Registering of the Data ##########
        result['Result'].columns=[str(position)+name]
        
        ########## Plotting for the option ##########
        #grapher(result['Result'],name=result['Result'].columns[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        return result
    
    def digital_opt(self,historicVol=None,strike=None,min_val_graph=15,max_val_graph=26,position=None,digital_type=None,payoff=None,
                     spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        ''' digital_opt contruction of the digital option type, returns strategy object 
            digital type 1 call 
            digital type -1 put
            
            Parameters
            ----------
            historicVol       : dict
            strike            : float
            min_val_graph     : int
            max_val_graph     : int
            position          : int
            digital_type      : int
            payoff            : int
            spot              : float
            domesticRate      : dict
            foreignRate       : dict
            days_maturity     : int
            volatility        : float 
            disable           : int (boolean)
            
        '''
        ########## Validation to proceed ##########
        strike,position,payoff=validation(strike,position,payoff)
        notional=payoff/.05
        ########## Digital ##########
        if digital_type==1:
            if position==-1:
                opt=self.spread_opt(historicVol=historicVol,strike=[strike,strike+.05],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,spread_type=1,amount=[notional,notional],
                                    spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            else:
                opt=self.spread_opt(historicVol=historicVol,strike=[strike-.05,strike],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,spread_type=1,amount=[notional,notional],
                                    spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            name=' call'
        else:
            if position==-1:
                opt=self.spread_opt(historicVol=historicVol,strike=[strike,strike-.05],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,spread_type=-1,amount=[notional,notional],
                                    spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            else:
                opt=self.spread_opt(historicVol=historicVol,strike=[strike+.05,strike],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,spread_type=-1,amount=[notional,notional],
                                    spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            name=' put'
        ########## Registering of the Data ##########
        result=opt
        result['Result'].columns=['digital '+str(position)+name]
        ########## Plotting for the option ##########
        #grapher(result['Result'],name=result['Result'].columns[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        return result
    
    def digitalSpread_opt(self,historicVol=None,strike=[None,None],min_val_graph=15,max_val_graph=26,position=None,digitalS_type=None,payoff=None,
                          spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        ''' digitalSpread_opt contruction of the digital spread option type, returns strategy object, used mainly for internal options construction 
            DigitalS_type 1 for call spread
            DigitalS_type -1 for put spread
            
            Parameters
            ----------
            historicVol       : dict
            strike            : array of float
            min_val_graph     : int
            max_val_graph     : int
            position          : int
            digitalS_type     : int
            payoff            : int
            spot              : float
            domesticRate      : dict
            foreignRate       : dict
            days_maturity     : int
            volatility        : float 
            disable           : int (boolean)
            
        '''
        ########## Validation to proceed ##########
        strike,position,payoff=validation(strike,position,payoff)
        
        ########## Spread ##########
        opt1=self.digital_opt(historicVol=historicVol,strike=strike[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,digital_type=digitalS_type,payoff=payoff,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
        opt2=self.digital_opt(historicVol=historicVol,strike=strike[1],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1*position,digital_type=digitalS_type,payoff=payoff,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
        
        ########## Registering of the Data ##########
        result=self.join_strategy([opt1,opt2])
        name=str(position)+' D spread'+str(strike[0])+'-'+str(strike[1])
        result['Result'].columns=[name]
        ########## Plotting for the option ##########
        #grapher(result['Result'],name=result['Result'].columns[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        
        return result
    
    def touch_opt(self,historicVol=None,strike=[None,None],min_val_graph=15,max_val_graph=26,position=None,touch_type=None,payoff=None,
                     spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        ''' touch_opt contruction of the touch option type, returns strategy object 
            touch type -1 No touch 
            touch type 1 touch
            
            Parameters
            ----------
            historicVol       : dict
            strike            : array of float
            min_val_graph     : int
            max_val_graph     : int
            position          : int
            touch_type        : int
            payoff            : int
            spot              : float
            domesticRate      : dict
            foreignRate       : dict
            days_maturity     : int
            volatility        : float 
            disable           : int (boolean)
            
        '''
        
        ########## Validation to proceed ##########
        strike1,position,payoff=validation(strike[0],position,payoff)
        strike2,position,payoff=validation(strike[1],position,payoff)
        if touch_type==-1:
            opt1=self.barrier_opt(historicVol=historicVol,strike=strike1,barrier=strike2,position=position,option_type=1,barrier_type=touch_type,amount=payoff/np.abs(strike2-strike1),spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt2=self.barrier_opt(historicVol=historicVol,strike=strike2,barrier=strike1,position=position,option_type=-1,barrier_type=touch_type,amount=payoff/np.abs(strike2-strike1),spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            result=self.join_strategy([opt1,opt2])
        else:
            opt1=self.barrier_opt(historicVol=historicVol,strike=strike1,barrier=strike2,position=position,option_type=1,barrier_type=touch_type,amount=payoff/np.abs(strike2-strike1),spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt2=self.barrier_opt(historicVol=historicVol,strike=strike2,barrier=strike1,position=position,option_type=-1,barrier_type=touch_type,amount=payoff/np.abs(strike2-strike1),spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt3=self.call_opt(historicVol=historicVol,strike=strike2,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1*position,amount=payoff/np.abs(strike2-strike1),spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            opt4=self.put_opt(historicVol=historicVol,strike=strike1,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1*position,amount=payoff/np.abs(strike2-strike1),spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            result=self.join_strategy([opt1,opt2,opt3,opt4])
        ############## DNT otra manera ############       
        #touch
#        opt1=myoptioner.digital_opt(strike=18.9,position=position,digital_type=-1,payoff=1)
#        opt2=myoptioner.digital_opt(strike=21.1,position=position,digital_type=1,payoff=1)
#        s=myoptioner.join_strategy([opt1,opt2])
#        # no touch
#        position=1
#        opt1=myoptioner.digital_opt(strike=18.9,position=-1*position,digital_type=-1,payoff=.5)
#        opt2=myoptioner.digital_opt(strike=18.9,position=position,digital_type=1,payoff=.5)
#        opt3=myoptioner.digital_opt(strike=21.1,position=-1*position,digital_type=1,payoff=.5)
#        opt4=myoptioner.digital_opt(strike=21.1,position=position,digital_type=-1,payoff=.5)
#        s=myoptioner.join_strategy([opt1,opt2,opt3,opt4])

        ###########################################
        
        name=str(position)+' '+str(touch_type)+' Touch'
        ########## Registering of the Data ##########
        result['Result'].columns=[name]
        ########## Plotting for the option ##########
        #grapher(result['Result'],name=result['Result'].columns[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        return result
    
    def weddingCake_opt(self,historicVol=None,strikes=None,min_val_graph=15,max_val_graph=26,position=None,wedding_type=None,payoff=None,
                     spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        ''' weddingCake_opt contruction of the wedding cake option type, returns strategy object 
            strikes are set by levels first two positions of the vector are for the first level and so on,
            same for the volatilitys
            wedding type -1 No touch 
            wediing type 1 touch
            
            Parameters
            ----------
            historicVol       : dict
            strikes           : array of float
            min_val_graph     : int
            max_val_graph     : int
            position          : int
            wedding_type      : int
            payoff            : int
            spot              : float
            domesticRate      : dict
            foreignRate       : dict
            days_maturity     : int
            volatility        : float 
            disable           : int (boolean)
            
        '''
        
        ########## Validation to proceed ##########
        opt=self.touch_opt(historicVol=historicVol,strike=[strikes[0],strikes[1]],position=position,touch_type=wedding_type,payoff=payoff,
                          spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
        result=self.join_strategy([opt])
        for element in np.arange(int(len(strikes)/2-1)):
            opt=self.touch_opt(historicVol=historicVol,strike=[strikes[element*2+2],strikes[element*2+3]],position=position,touch_type=wedding_type,payoff=payoff,
                          spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
            result=self.join_strategy([result,opt])
        ########## Registering of the Data ##########
        name=str(position)+' WedCa '+str(len(strikes)/2)+' levels'
        result['Result'].columns=[name]
        ########## Plotting for the option ##########
        #grapher(result['Result'],name=result['Result'].columns[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        return result
    
    def racc_opt(self,historicVol=None,strike=[None,None],min_val_graph=15,max_val_graph=26,position=None,revisions=None,amount=None,digitalS_type=1,
                          spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        ''' racc_opt contruction of the racc option type, returns strategy object 
            Revisions must be a string D for daily or multiples of 7 (7D,14D...etc.)
            for days_maturity it can be any number for daily revisions for any other type or revisions days must also be a multiple of 7
            spreads are currently made with digital call spreads 
            
            Parameters
            ----------
            historicVol       : dict
            strike            : array of float
            min_val_graph     : int
            max_val_graph     : int
            position          : int
            revisions         : str
            amount            : int
            digitalS_type     : int
            spot              : float
            domesticRate      : dict
            foreignRate       : dict
            days_maturity     : int
            volatility        : float 
            disable           : int (boolean)
            
        '''
        ########## Validation to proceed ##########
        days=days_to_maturitys(days_maturity=days_maturity,revisions=revisions)
        opt=self.digitalSpread_opt(historicVol=historicVol,strike=[strike[0],strike[1]],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,digitalS_type=1,payoff=amount/len(days),spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days[0],volatility=volatility,disable=disable)
        result=self.join_strategy([opt])
        for term in days[1:]:
            opt=self.digitalSpread_opt(historicVol=historicVol,strike=[strike[0],strike[1]],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,digitalS_type=1,payoff=amount/len(days),spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=term,volatility=volatility,disable=disable)
            result=self.join_strategy([result,opt])
            
        ########## Registering of the Data ##########
        name=str(position)+' RACC '+str(strike[0])+'-'+str(strike[1])+' rev'+str(revisions)
        result['Result'].columns=[name]
        ########## Plotting for the option ##########
        #grapher(result['Result'],name=result['Result'].columns[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        return result
    
    #Second generation exotics
    
    def corridor_opt():
        ''' 
    
        '''
        return
    
    def fader_opt():
        ''' 
        faders son un conjunto de raccs  o de eko call & eko puts
        '''
        return
    
    ################################### Structured products ###############################################
    
    def openForward():
        ''' 
    
        '''
        #tienes hasta la fecha del forward para quemar el monto del forward, no estas amarrado a cada fecha pero al final tiene que haberse usado todo
        return
    
    def cross_currency():
        ''' 
    
        '''
        return
    
    def outrightForward_opt():
        ''' 
    
        '''
        return
    
    def participatingForward_opt():
        ''' 
    
        '''
        return
        
    def boomerangForward_opt():
        ''' 
    
        '''
        return
    
    def sharkForward_opt():
        ''' 
    
        '''
        return
    
    def KIKOFforward_opt(self,historicVol=None,strike=None,barrier=None,min_val_graph=15,max_val_graph=26,position=None,barrier_type=None,amount=None,
                          spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        ''' KIKOFforward_opt contruction of the forward with barriers strategy type, returns strategy object 
            Barrier type 1 KI
            Barrier type -1 KO
            for zero cost product the barrier must be a some below level
            
            Parameters
            ----------
            historicVol       : dict
            strike            : float
            barrier           : float
            min_val_graph     : int
            max_val_graph     : int
            position          : int
            barrier_type      : int
            amount            : int
            spot              : float
            domesticRate      : dict
            foreignRate       : dict
            days_maturity     : int
            volatility        : float 
            disable           : int (boolean)
            
        '''
        opt1=self.barrier_opt(historicVol=historicVol,strike=strike,barrier=barrier,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,option_type=-1,barrier_type=barrier_type,amount=1,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
        opt2=self.barrier_opt(historicVol=historicVol,strike=strike,barrier=barrier,min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1*position,option_type=1,barrier_type=barrier_type,amount=1,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
        result=self.join_strategy([opt1,opt2])
        
        ########## Registering of the Data ##########
        name=str(position)+' '+str(barrier_type)+' forward '+str(strike)+' b '+str(barrier)
        result['Result'].columns=[name]
        ########## Plotting for the option ##########
        #grapher(result['Result'],name=result['Result'].columns[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        return result
    
    def RangeForward_opt():
        ''' 
    
        '''
        return
    
    def RACCForward_opt(self,historicVol=None,strike=[None,None,None],min_val_graph=15,max_val_graph=26,position=None,revisions=None,amount=None,digitalS_type=1,
                          spot=.1,domesticRate=.1,foreignRate=.1,days_maturity=.1,volatility=5/10000,disable=1):
        ''' RACCForward_opt contruction of the racc forward strategy type, returns strategy object 
            strike 1 - forward strike
            strike 2 & 3 RACC's range
            same for volatilitys, teorically for zero cost product the forward strike must be below the range
            same rules for RACC opt:
                Revisions must be a string D for daily or multiples of 7 (7D,14D...etc.)
                for days_maturity it can be any number for daily revisions for any other type or revisions days must also be a multiple of 7
                spreads are currently made with digital call spreads
            
            Parameters
            ----------
            historicVol       : dict
            strike            : array of float
            barrier           : float
            min_val_graph     : int
            max_val_graph     : int
            position          : int
            revisions         : str
            amount            : int
            digitalS_typ      : int
            spot              : float
            domesticRate      : dict
            foreignRate       : dict
            days_maturity     : int
            volatility        : float 
            disable           : int (boolean)
            
        '''
                    
        opt1=self.put_opt(historicVol=historicVol,strike=strike[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,amount=amount,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
        opt2=self.call_opt(historicVol=historicVol,strike=strike[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=-1*position,amount=amount,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
        opt3=self.racc_opt(historicVol=historicVol,strike=[strike[1],strike[2]],min_val_graph=min_val_graph,max_val_graph=max_val_graph,position=position,revisions=revisions,amount=amount,digitalS_type=digitalS_type,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=disable)
        result=self.join_strategy([opt1,opt2,opt3])
        
        ########## Registering of the Data ##########
        name=str(position)+' RACCForward '+str(strike[0])+' '+str(strike[1])+'-'+str(strike[2])+' rev'+str(revisions)
        result['Result'].columns=[name]
        ########## Plotting for the option ##########
        #grapher(result['Result'],name=result['Result'].columns[0],min_val_graph=min_val_graph,max_val_graph=max_val_graph)
        return result
    
    ################################### compound products ###############################################
    #call sobre call
    #call sobre put
    #put sobre call
    #put sobre put
    
    #necesitas colcular FVA (forward de volatilidad) para hacer el hedge (necesitas comprar el subyacente)
    

#%%
####################################################### Testing #################################################################
if __name__ == '__main__':
    idInstrument=['NI7401479','NI379287','NI379288','NI379289','NI379290']
    foreignRate=list(map(lambda instrument:list(bloombergData.aggregate([{'$match':{'instruments.idBBUnique':instrument}},{'$project': {'metadata': {'$filter': {'input': "$instruments",'as': "instrument",'cond': { '$or': [{'$eq': [ "$$instrument.idBBUnique",instrument]}]}}}}}]))[-1],idInstrument))        
    idInstrument=['NI530453','NI529970','NI41699033']
    domesticRate=list(map(lambda instrument:list(bloombergData.aggregate([{'$match':{'instruments.idBBUnique':instrument}},{'$project': {'metadata': {'$filter': {'input': "$instruments",'as': "instrument",'cond': { '$or': [{'$eq': [ "$$instrument.idBBUnique",instrument]}]}}}}}]))[-1],idInstrument))
    spot=get_spot()['Close']
    volatility = 5/10000
    strike = 19.01
    days_maturity=30
    #
    myoptioner=options()
    
    #test vol functions
    domesticRate=yield_interpolation(yieldCurve=domesticRate,Term=days_maturity)
    foreignRate=yield_interpolation(yieldCurve=foreignRate,Term=days_maturity)
    #%
    historicVol=global_vol(conection=1,underlying='USD_MXN',collection=bloombergData)
    askcallStrikes,askputStrikes,bidcallStrikes,bidputStrikes,asksmile,bidsmile,elapsed_time_int=strike_smile_tables(historicVol=historicVol,foreignRate=foreignRate,domesticRate=domesticRate,spot=spot)
    #%
    strike = 19.1
    days_maturity=28
    callvol=asksmile.iloc[pd.Series(np.abs(askcallStrikes.index-days_maturity)).idxmin()][pd.Series(np.abs(askcallStrikes.iloc[pd.Series(np.abs(askcallStrikes.index-days_maturity)).idxmin()]-strike)).idxmin()]+volatility
    putvol=asksmile.iloc[pd.Series(np.abs(askputStrikes.index-days_maturity)).idxmin()][pd.Series(np.abs(askputStrikes.iloc[pd.Series(np.abs(askputStrikes.index-days_maturity)).idxmin()]-strike)).idxmin()]+volatility
    #%

    #test pricing functions
    prime=BlsPrice(spot=spot,strike=strike,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=[callvol,putvol],yer_periodicity=360)
    delta=BlsDelta(spot=spot,strike=19,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=[callvol,putvol])
    gamma=BlsGamma(spot=spot,strike=19,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=callvol)
    theta=BlsTheta(spot=spot,strike=19,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=[callvol,putvol])
    vega=BlsVega(spot=spot,strike=19,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=callvol)
    
    #test diferent options
    call=myoptioner.call_opt(strike=19,position=1,amount=1)
    put=myoptioner.put_opt(strike=19,position=1,amount=10)
    
    #test vanilla pricing 
    call=myoptioner.call_opt(historicVol=historicVol,position=1,amount=1,strike=strike,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=0)
    #whithout passing all the arguments
    call=myoptioner.call_opt(historicVol=historicVol,position=1,amount=1,strike=strike)

    #test joining strategys
    total=myoptioner.join_strategy([call,put])
    
    #Testing reevaluation with a change in strategy's status 
    test=myoptioner.join_strategy([call,put,put,call])
    #recalculate whitout changing
    temp=myoptioner.recalculate(test)
    #changing all status
    temp=myoptioner.recalculate(test,change_status=1)
    #changing specific status
    temp=myoptioner.recalculate(test,change_status=1,which_component=[0,2])
    
    #Testing spread options
    #spread=myoptioner.spread_opt(strike=[19,21],spread_type=1,amount=[1,1])
    #Testing pricing spread
    spread=myoptioner.spread_opt(historicVol=historicVol,strike=[19.0,21.0],position=1,spread_type=1,amount=[1,1],spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=0)
    
    #Testing strangle options
    #teorically the first strike has to be bigger 
    strangle=myoptioner.straddle_opt(strike=[21,19],position=-1,amount=[1,1])
    #strangle=myoptioner.straddle_opt(historicVol=historicVol,strike=[21.0,19.0],position=-1,amount=[1,1],spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=0)
    
    #straddle has the same strike for both
    straddle=myoptioner.straddle_opt(strike=[19],position=1,amount=[1,1])
    #straddle=myoptioner.straddle_opt(historicVol=historicVol,strike=[19.0],position=1,amount=[1,1],spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=0)
    
    #Testing butterfly 
    butterfly=myoptioner.butterfly_opt(strike=19.07,spread=1.6,position=1,butterfly_type=4,amount=1)
    #butterfly=myoptioner.butterfly_opt(historicVol=historicVol,strike=19.07,spread=1.6,butterfly_type=4,amount=1,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=0)
    
    #Testing Seagull
    seagull=myoptioner.seagull_opt(strike=[19,20,18],position=1,seagull_type=1,amount=[1,1,1])
    #seagull=myoptioner.seagull_opt(historicVol=historicVol,strike=[19.0,18.0,20.0],seagull_type=2,amount=[1,1,1],spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=0)
    
    #Testing risk reversal
    riskReversal=myoptioner.riskReversal_opt(strike=[19.8,19],position=1,riskReversal_type=2,amount=[1,1])
    #riskReversal=myoptioner.riskReversal_opt(historicVol=historicVol,strike=[19.8,19.0],riskReversal_type=1,amount=[1,1],spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=0)
    
    #Testing Digitals
    digitalC=myoptioner.digital_opt(strike=19,position=1,digital_type=1,payoff=1)
    #digitalC=myoptioner.digital_opt(historicVol=historicVol,strike=19.0,position=1,digital_type=1,payoff=1,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=0)
    
    #Testing Barriers
    RKI_call=myoptioner.barrier_opt(strike=19,barrier=20,position=1,option_type=1,barrier_type=1,amount=1)
    #RKI_call=myoptioner.barrier_opt(historicVol=historicVol,strike=19.0,barrier=20.0,position=1,option_type=1,barrier_type=1,amount=1,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=0)
    
    #Testing Touchs
    DNT=myoptioner.touch_opt(strike=[18.90,21.10],position=1,touch_type=-1,payoff=1)
    #DNT=myoptioner.touch_opt(historicVol=historicVol,strike=[18.90,21.10],position=1,touch_type=-1,payoff=1,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=0)
    
    #Testing Wedding Cakes
    WC=myoptioner.weddingCake_opt(strikes=[19.0,21.0,19.5,20.5],position=1,wedding_type=-1,payoff=1)
    #WC=myoptioner.weddingCake_opt(historicVol=historicVol,strikes=[19.0,21.0,19.5,20.5],position=1,wedding_type=-1,payoff=1,spot=spot,domesticRate=domesticRate,foreignRate=foreignRate,days_maturity=days_maturity,volatility=volatility,disable=0)
    
    #RACC Testing
    RACC=myoptioner.racc_opt(historicVol=historicVol,strike=[19,20],position=1,revisions='14D',days_maturity=28,amount=1)
    
    #Testing KIKOForward
    KOForward=myoptioner.KIKOFforward_opt(historicVol=historicVol,strike=19.5,barrier=19.45,position=1,barrier_type=-1,amount=1)
    
    #Testing RACCForward
    RACCForward=myoptioner.RACCForward_opt(historicVol=historicVol,strike=[18.4,18.7,19.3],position=1,amount=1,days_maturity=7,revisions='D')