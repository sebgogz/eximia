import { gql } from 'apollo-server';

export default gql`
  type Account {
    id: String @takeFrom(key: "_id")
    clientId: Int
    clientName: String
    clientLastN: String
    clientMail: String
    clientPhone: String
    accountInfo: [AccountInfo]
  }

  type AccountInfo {
    accountId: Int
    balance: Float
    rend: String
    strategy: Strategy
    book: [Book]
  }

  type Strategy {
    result: StrategyResult @takeFrom(key: "Result")
    primeResult: Float @takeFrom(key: "Prime_result")
    deltaResult: Float @takeFrom(key: "Delta_result")
    df: StrategyResult @takeFrom(key: "DF")
    prime: [Float]
    status: [Int]
    delta: [Float]
    gamma: [Float]
    theta: [Float]
    vega: [Float]
    life: [Float]
    type: [Int] @takeFrom(key: "Type")
    strike: [Float] @takeFrom(key: "Strike")
    amount: [Float] @takeFrom(key: "Amount")
  }

  type StrategyResult {
    index: [Float]
    columns: [String]
    data: [[Float]]
  }

  type Book {
    oid: Int @takeFrom(key: "OID")
    underlying: String
    position: Float
    type: Float @takeFrom(key: "Type")
    strike: Float
    term: Float
    amount: Float
    prime: Float
    delta: Float
    gamma: Float
    theta: Float
    vega: Float
    life: Int
    cp: Int @takeFrom(key: "c_p")
    date: BookDate
    expDate: BookDate @takeFrom(key: "exp_date")
  }

  type BookDate {
    date: Date @takeFrom(key: "$date")
  }

  extend type Query {
    getAccount: Account
    getAccounts: [Account]
  }
`;
