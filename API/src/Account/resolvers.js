import account from './model';
import me from '../../documents/me';
import accounts from '../../documents/accounts';

export default {
  Query: {
    getAccount: () => {
      return me;
    },
    getAccounts: () => {
      return accounts;
    }
    /*
    getAccounts: async (_source, _args, _context) => {
      const accounts = await account.find({});
      return accounts;
    },
    */
  },
};
