import mongoose from 'mongoose';

const accountSchema = new mongoose.Schema({
  clientId: {
    type: Number,
  },
  clientLastN: {
    type: Number,
  },
});

const account = mongoose.model('account', accountSchema);

export default account;
