import { gql } from 'apollo-server';

export default gql`
  directive @takeFrom(key: String) on FIELD_DEFINITION
  scalar Date

  type Query {
    _empty: String
  }
  type Mutation {
    _empty: String
  }
`;
