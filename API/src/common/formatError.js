const formatError = (err) => {
  // we check if there was a response from the data source, and build our error object
  if (err.extensions.response && err.extensions.response.body) {
    const message = err.extensions.response.body.message || err.extensions.response.body.error;
    const field = err.extensions.response.body.fields || '';
    return {
      message: `${message}${field}`,
      type: err.extensions.response.body.type,
      code: err.extensions.code,
      status: err.extensions.response.status || 404,
    };
  }

  // Otherwise return the original error message and extensions.
  const { message, extensions } = err;
  return { message, extensions };
};

export default formatError;
