import dotenv from 'dotenv';

dotenv.config();

const CONCURRENCIES_DB = {
  DB_HOST: process.env.DB_HOST,
  DB_USER: process.env.DB_USER,
  DB_PASSWORD: process.env.DB_PASSWORD,
};

const environments = {
  development: {
    API_URL: 'http://backend:3000/api/',
    ...CONCURRENCIES_DB,
  },
  staging: {
    API_URL: 'http://backend:3000/api/',
    ...CONCURRENCIES_DB,
  },
  production: {
    API_URL: 'http://backend:3000/api/',
    ...CONCURRENCIES_DB,
  },
};

export default environments[process.env.MODE] || environments.development;
