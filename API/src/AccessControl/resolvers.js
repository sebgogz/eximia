export default {
  Query: {
    getUser: (_source, _args, { dataSources }) => {
      return dataSources.accessControlAPI.getUser();
    },
  },
  Mutation: {
    login: (_source, { params }, { dataSources }) => {
      const { email, password } = params;

      return dataSources.accessControlAPI.login(email, password);
    },
    register: (_source, { params }, { dataSources }) => {
      const { email, password, role } = params;

      return dataSources.accessControlAPI.register(email, password, role);
    },
  },
};
