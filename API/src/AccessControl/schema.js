import { gql } from 'apollo-server';

export default gql`
  type UserInfo {
    auth: String
    token: String
    message: String
  }

  input LoginParams {
    email: String!
    password: String!
  }

  input RegisterParams {
    email: String!
    password: String!
    role: String!
  }

  extend type Query {
    getUser: User
  }

  extend type Mutation {
    login(params: LoginParams!): UserInfo
    register(params: RegisterParams!): User
  }
`;
