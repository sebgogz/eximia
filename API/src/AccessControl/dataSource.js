import { RESTDataSource } from 'apollo-datasource-rest';
import config from '../config';

class AccessControlAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = config.API_URL;
  }

  willSendRequest(request) {
    request.headers.set('Content-Type', 'application/json');
    request.headers.set('Authorization', this.context.token);
  }

  getUser() {
    return this.get('user');
  }

  async login(email, password) {
    return this.post('login', { email, password });
  }

  async register(email, password, role) {
    return this.post('register', { email, password, role });
  }
}

export default AccessControlAPI;
