import { gql } from 'apollo-server';

export default gql`
  type User {
    id: String!
    role: String!
    email: String!
    createdAt: Date!
    updatedAt: Date!
  }

  extend type Query {
    getUsers: [User]
  }
`;
