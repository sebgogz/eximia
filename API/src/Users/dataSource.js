import { RESTDataSource } from 'apollo-datasource-rest';
import config from '../config';

class UsersAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = config.API_URL;
  }

  willSendRequest(request) {
    request.headers.set('Content-Type', 'application/json');
    request.headers.set('Authorization', this.context.token);
  }

  getUsers() {
    return this.get('users');
  }
}

export default UsersAPI;
