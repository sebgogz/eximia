export default {
  Query: {
    getUsers: (_source, _args, { dataSources }) => (
      dataSources.usersAPI.getUsers()
    ),
  },
};
