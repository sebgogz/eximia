// @flow

import { SchemaDirectiveVisitor } from 'graphql-tools';
import { defaultFieldResolver } from 'graphql';

export default class TakeFrom extends SchemaDirectiveVisitor {
  visitFieldDefinition(field) {
    const { key } = this.args;
    const { resolve = defaultFieldResolver } = field;

    field.resolve = async (object, args, context, info) => {
      object[field.name] = object[key];
      return resolve.call(this, object, args, context, info);
    };
  }
}
