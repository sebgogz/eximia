import { makeExecutableSchema, addMockFunctionsToSchema } from 'graphql-tools';

import {
  commonSchema,
  formatError,
} from './common';
import {
  AccessControlAPI,
  accessControlResolvers,
  accessControlSchema,
} from './AccessControl';
import {
  UsersAPI,
  usersResolvers,
  usersSchema,
} from './Users';
import {
  accountSchema,
  accountResolvers,
} from './Account';
import TakeFrom from './directives/TakeFrom';

// set typeDefs, resolvers and dataSources to the schema
const schema = makeExecutableSchema({
  typeDefs: [
    commonSchema,
    accessControlSchema,
    usersSchema,
    accountSchema,
  ],
  resolvers: [
    accessControlResolvers,
    usersResolvers,
    accountResolvers,
  ],
  schemaDirectives: {
    takeFrom: TakeFrom,
  },
});

export default {
  schema,
  dataSources: () => ({
    usersAPI: new UsersAPI(),
    accessControlAPI: new AccessControlAPI(),
  }),
  context: ({ req }) => {
    const token = req.headers.authorization || '';

    return {
      token,
    }
  },
  formatError,
};
