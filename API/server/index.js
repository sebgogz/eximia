import { ApolloServer } from 'apollo-server';

/*
import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
*/

import  serverConfig from '../src/serverConfig';
//import config from '../src/config';

// const app = express();
// app.use(cors());

const server = new ApolloServer({...serverConfig, cors: { origin: true, credentials: true } });

// server.applyMiddleware({ app, path: '/graphql' });

server.listen({ port: 3001 }).then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});

/*
app.listen(3001, () => {
  mongoose.connect(
    'mongodb://automaticForexScript:lkj031j3r1-.ajs.13r1@134.209.74.56:27017/admin',
    // `mongodb//${config.DB_USER}:${config.DB_PASSWORD}@${config.DB_HOST}:${config.DB_PORT}`,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  ).then(
    () => console.log('success'),
    err => console.log(err)
  );
  console.log('🚀 Server ready at port: 3001');
});
*/
