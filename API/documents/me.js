const me = {
  "_id" : "5e001ab140f2aa015a59b5c5",
  "clientId" : 1,
  "clientName" : "test 1",
  "clientLastN" : "a",
  "clientMail" : "a",
  "clientPhone" : "a",
  "accountInfo" : [
    {
      "accountId" : 1,
      "balance" : -590.1658149627616,
      "rend" : null,
      "strategy" : {
        "Result" : {
          "index" : [ 15.0, 15.01, 15.02, 15.03, 15.04 ],
          "columns" : [ "strategy" ],
          "data" : [ [ -190.16581496276163 ] ]
        },
        "Prime_result" : -590.1658149627616,
        "Delta_result" : 1572.8776909959042,
        "DF" : {
          "index" : [ 15.0, 15.01, 15.02, 15.03, 15.04 ],
          "columns" : [ "P&L 1.0 call 19.2" ],
          "data" : [
            [ -190.16581496276163 ],
            [ 109.83418503636156 ],
            [ 209.83418503634167 ],
            [ 309.83418503632174 ]
          ]
        },
        "prime": [-190.16581496276163],
        "status": [1],
        "delta": [1572.8776909959042],
        "gamma": [1.0370199111938174],
        "theta": [-16218.45721219345],
        "vega": [0.6353797616074639],
        "life": [7.0],
        "Type": [1],
        "Strike": [19.2],
        "Amount": [10000.0]
      },
      "book": [{
        "OID": 1,
        "underlying": "USD/MXN",
        "position": -1.0,
        "Type": 0,
        "strike": 19.2,
        "term": 7.0,
        "amount": 10000.0,
        "prime": -4.64695109072367E-4,
        "delta": 0.0380163445899664,
        "gamma": 2.9864755377593936E-4,
        "theta": -0.6817163533108663,
        "vega": 3.0739248460922455E-5,
        "date": {
          "$date": "2019-12-23T09:55:33.898+0000"
        },
        "exp_date": {
          "$date": "2019-12-30T09:55:33.898+0000"
        },
        "life": 0,
        "c_p": 1
      }]
    }
  ]
};

export default me;
