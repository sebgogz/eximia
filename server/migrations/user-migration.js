// user-migration.js
const ROLES = [
  'admin',
  'editor',
  'normal'
];

module.exports = {
  up: (queryInterface, Sequelize) =>
      queryInterface.createTable('Users', {
          id: {
              allowNull: false,
              unique: true,
              autoIncrement: true,
              primaryKey: true,
              type: Sequelize.INTEGER
          },
          role: {
              allowNull: false,
              type: Sequelize.ENUM(ROLES)
          },
          email: {
              allowNull: false,
              unique: true,
              type: Sequelize.STRING
          },
          password: {
              allowNull: false,
              type: Sequelize.STRING
          },
          createdAt: {
              allowNull: false,
              type: Sequelize.DATE
          },
          updatedAt: {
              allowNull: false,
              type: Sequelize.DATE
          }
      }),
  down: (queryInterface /* , Sequelize */) => queryInterface.dropTable('Users')
};
