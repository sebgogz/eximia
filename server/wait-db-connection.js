const {Sequelize} = require('sequelize');

const config = require('./config/config')['production'];

// Option 1: Passing parameters separately
const sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    dialect: config.dialect
});

async function loopUntilDBFound() {
    let dbFound = false;

    while (!dbFound) {
        try {
            await sequelize.authenticate();
            console.log('Connection has been established successfully, db is up');
            dbFound = true;
        } catch (e) {
            console.error('Unable to connect to the database, retrying in 3s');
            await new Promise(function (resolve) {
                setTimeout(resolve, 3000);
            });
        }
    }
}

loopUntilDBFound().then(() => console.log("Closing script"));
