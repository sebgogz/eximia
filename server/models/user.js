//USER Schema
import bcrypt from 'bcrypt';

const BCRYPT_SALT_ROUNDS = 12;
const ROLES = [
  'admin',
  'editor',
  'normal',
];

module.exports = (sequelize, DataTypes) => {
  let User = sequelize.define('User', {
    email: {
      unique: true,
      allowNull: false,
      type: DataTypes.STRING,
    },
    role: {
      allowNull: false,
      type: DataTypes.ENUM(ROLES),
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    hooks: {
      beforeCreate: (user) => {
        return bcrypt.hash(user.password, BCRYPT_SALT_ROUNDS).then(hashedPassword => {
          return user.password = hashedPassword;
        });
      }
    }
  });
  return User;
}
