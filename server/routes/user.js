const userController = require('../controllers').user;
import passport from 'passport';

const checkIsInRole = (...roles) => (req, res, next) => {
  if (!req.user) {
    console.log('NOT AUTHORIZED FOR THIS USER');
    return res.status(401).send({ message: 'UNAUTHORIZED' });
  }

  const hasRole = roles.find(role => req.user.role === role)
  if (!hasRole) {
    console.log('NOT AUTHORIZED FOR THIS USER');
    return res.status(401).send({ message: 'UNAUTHORIZED' });
  }

  return next();
}

module.exports = (app) => {
  app.get('/api', (req, res) => {
    res.status(200).send({
      data: "Welcome Node Sequlize API v1"
    })
  })

  app.get(
    '/api/user',
    passport.authenticate('jwt', { session: false }),
    userController.getUser
  );
  app.get(
    '/api/users',
    passport.authenticate('jwt', { session: false }),
    checkIsInRole('admin'),
    userController.getAllUsers,
  );
  app.post(
    '/api/register',
    passport.authenticate('jwt', { session: false }),
    checkIsInRole('admin'),
    userController.register,
  );
  app.post('/api/login', userController.login);
  app.get('/api/logout', userController.logout);
}
