const User = require('../models').User;
import passport from 'passport';
import jwt from 'jsonwebtoken';
import jwtSecret from '../config/jwtConfig';
import Sequelize from 'sequelize';

const Op = Sequelize.Op;

module.exports = {
  async getUser(req, res) {
    if (req.user) {
      const { user } = req;
      const userInfo = {
        id: user.id,
        email: user.email,
        role: user.role,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt,
      };
      res.status(200).send(userInfo);
    } else {
      res.status(404).send({ message: 'user not logged in' });
    }
  },

  async getAllUsers(req,res) {
    try {
      const userCollection = await User.findAll({
        where: {
          email: {
            [Op.ne]: req.user.email,
          },
        },
      });
      res.status(200).send(userCollection);
    }
    catch(e){
      res.status(500).send(e);
    }
  },

  async register(req, res, next) {
    const { body } = req;
    const { email, password, role } = body;

    if (!email || !password || !role) {
      res.status(422).send({ message: 'No field can be empty' });
    }

    User.findOne({
      where: {
        email
      },
    }).then(user => {
      if (user) {
        console.log('Email already taken');
        return res.status(422).send({ message: 'Email already taken' });
      } else {
        User
          .create({ email, password, role })
          .then(user => {
            console.log('user registered');
            const userInfo = {
              id: user.id,
              email: user.email,
              role: user.role,
              updatedAt: user.updatedAt,
              createdAt: user.createdAt,
            };
            return res.status(200).send(userInfo);
          });
      }
    }).catch(err => res.status(500).send({ message: 'algo salió mal', err }));
  },

  async login(req, res, next) {
    passport.authenticate('login', { session: false }, (err, user, info) => {
      if (err) {
        console.log('LoginController: -> err', err);
      }

      if (info !== undefined) {
        console.log('LoginController -> info', info);
        res.send(info.message);
      } else {
        req.login(user, (err) => {
          if (err) {
            return next(err);
          }

          User.findOne({
            where: {
              email: user.email,
            },
          }).then(user => {
            const token = jwt.sign({ id: user.email }, jwtSecret.secret, { expiresIn: 60 * 60 });
            res.status(200).send({
              auth: true,
              token,
              message: 'user found & logged in',
            });
          });
        });
      }
    })(req, res, next);
  },

  async logout(req, res) {
    if (req.logout) {
      req.logout();
    }
    res.status(200).send({ message: 'true' });
  }
}
