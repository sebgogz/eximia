'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      id: 1,
      role: 'admin',
      email: 'admin@demo.com',
      password: '$2b$12$e9bHtrUTbvyyCTMV0VibzOa9aT8Nv1JmsWJs8.EP4HLuAkyWn.IUe',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
