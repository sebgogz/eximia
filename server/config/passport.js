import passport from 'passport';
import PassportLocal from 'passport-local';
import PassportJwt from 'passport-jwt'
import jwtSecret from './jwtConfig';
import bcrypt from 'bcrypt';

const { Strategy: LocalStrategy } = PassportLocal;
const { Strategy: JWTStrategy, ExtractJwt } = PassportJwt;

import models from '../models';
const { User } = models;

passport.use(
  'login',
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      session: false,
    },
    (email, password, done) => {
      try {
        User.findOne({
          where: {
            email: email,
          },
        }).then(user => {
          if (user === null) {
            return done(null, false, { message: 'bad email' });
          } else {
            bcrypt.compare(password, user.password).then(response => {
              if (!response) {
                console.log('passwords do not match');
                return done(null, false, { message: 'passwords do not match' });
              }
              console.log('user found & authenticated');
              return done(null, user);
            });
          }
        });
      } catch (err) {
        done(err);
      }
    },
  ),
);

passport.serializeUser(function(user, done) {
  done(null, user.email);
});

passport.deserializeUser(function(user, done) {
  done(null, user.email);
});

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt'),
  secretOrKey: jwtSecret.secret,
  session: false,
};

passport.use(
  'jwt',
  new JWTStrategy(opts, (jwt_payload, done) => {
    try {
      User.findOne({
        where: {
          email: jwt_payload.id,
        },
      }).then(user => {
        if (user) {
          console.log('user found in db in passport');
          done(null, user);
        } else {
          console.log('user not found in db');
          done(null, false);
        }
      });
    } catch (err) {
      done(err);
    }
  }),
);
